; Sample program that reads one line from the user,
; and prints it back in upper case.
;
; License: CC0

.text
    lea rio, MESSAGE
    puts
    set rio, 0
    halt

.data
message:        .asciiz "Hello, world!\n"

.end

; vim: set ft=antz:
