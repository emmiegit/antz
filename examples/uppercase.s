; Sample program that reads one line from the user,
; and prints it back in upper case. Removes the last character
; when the backspace key is pressed.
;
; License: CC0

;---------------
; Register Map
;---------------
; rio - input / output
; r0  - start of buffer
; r1  - current position in buffer
; r2  - input character
; r3  - temporary
; r4  - which byte inside the word
; r5  - unfinished word

.text main
    lea rio, input_message
    puts
    ld r0, string_buffer
    ld r1, string_buffer
    ; goto loop

loop:
    getc
    putc

    mov r2, rio
    cmp r3, r2, '\x7f'
    jez r3, backspace

    cmp r3, r2, '\n'
    jez r3, finish

    and r2, r2, 0xdf        ; bitmask for uppercase conversion
    cmp r3, r2, 'A'
    jlz r3, store_char

    cmp r3, r2, 'Z'
    jgz r3, store_char
    
    mov rio, r2
    ; goto store_char

store_char:
    lshf rio, rio, 24
    rshf r5, r5, 8
    or r5, r5, rio          ; pack byte into word

    add r4, r4, 1
    cmp r3, r4, 3           ; determine if we can move to the next word yet
    jge push_char
    jmp loop

backspace:
    cmp r3, r0, r1
    jle loop

    rshf rio, rio, 24
    lshf r5, r5, 8
    
    add r4, r4, 3
    mod r4, r4, 4           ; undo the byte count rollover
    jmp loop

push_char:
    str r5, r0, 0

    add r1, r1, 1
    set r4, 0
    set r5, 0
    jmp loop

finish:
    set r1, 0
    str r1, r0, 0

print:
    set rio, '\n'
    putc

    lea rio, string_buf
    puts

    halt
    
.data
input_message:      .asciiz "Enter a message: " 
string_buffer:      .block 100

.end

; vim: set ft=antz:
