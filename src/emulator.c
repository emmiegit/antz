/*
 * emulator.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "arguments.h"
#include "assembly_util.h"
#include "emulator.h"
#include "info.h"
#include "journal.h"
#include "main.h"
#include "memory.h"
#include "opcode.h"

#define UNUSED(x)               ((void)(x))
#define STREMPTY(x)             ((x)[0] == '\0')
#define STREQUALS(x, y)         (strcmp((x), (y)) == 0)
#define STREQUALS2(x, y, l)     (strncmp((x), (y), (l)) == 0)
#define ASCIIFY(x)              (isprint(x) ? (x) : '.')

#define ARGUMENT_BUFFER_LENGTH  48

#define RED_BOLD_ANSI           "\033[31;1m"
#define GREEN_BOLD_ANSI         "\033[32;1m"
#define YELLOW_BOLD_ANSI        "\033[33;1m"
#define BLUE_BOLD_ANSI          "\033[34;1m"
#define MAGENTA_BOLD_ANSI       "\033[35;1m"
#define CYAN_BOLD_ANSI          "\033[36;1m"
#define WHITE_BOLD_ANSI         "\033[37;1m"
#define RED_ANSI                "\033[31m"
#define GREEN_ANSI              "\033[32m"
#define YELLOW_ANSI             "\033[33m"
#define BLUE_ANSI               "\033[34m"
#define MAGENTA_ANSI            "\033[35m"
#define CYAN_ANSI               "\033[36m"
#define WHITE_ANSI              "\033[37m"
#define ENDCOLOR_ANSI           "\033[0m"

#define RED_BOLD                (state->opt->colors ? RED_BOLD_ANSI : "")
#define GREEN_BOLD              (state->opt->colors ? GREEN_BOLD_ANSI : "")
#define YELLOW_BOLD             (state->opt->colors ? YELLOW_BOLD_ANSI : "")
#define BLUE_BOLD               (state->opt->colors ? BLUE_BOLD_ANSI : "")
#define MAGENTA_BOLD            (state->opt->colors ? MAGENTA_BOLD_ANSI : "")
#define CYAN_BOLD               (state->opt->colors ? CYAN_BOLD_ANSI : "")
#define WHITE_BOLD              (state->opt->colors ? WHITE_BOLD_ANSI : "")
#define RED                     (state->opt->colors ? RED_ANSI : "")
#define GREEN                   (state->opt->colors ? GREEN_ANSI : "")
#define YELLOW                  (state->opt->colors ? YELLOW_ANSI : "")
#define BLUE                    (state->opt->colors ? BLUE_ANSI : "")
#define MAGENTA                 (state->opt->colors ? MAGENTA_ANSI : "")
#define CYAN                    (state->opt->colors ? CYAN_ANSI : "")
#define WHITE                   (state->opt->colors ? WHITE_ANSI : "")
#define ENDCOLOR                (state->opt->colors ? ENDCOLOR_ANSI : "")

/* TODO add breakpoints */
#define INTERACTIVE_EMULATOR_HELP_STRING \
    "You only need to enter the minimum number of characters to unamibiguously identify\n" \
    "a command. For example, you only need to type \"q\" or \"qu\" for the emulator to\n" \
    "understand that you want to quit.\n" \
    "\nAlso note that pressing enter runs the previous execution command.\n" \
    "\nGeneral commands:\n" \
    "   help\n" \
    "     Print this message.\n" \
    "   quit\n" \
    "     Exit the emulator.\n" \
    "\nExecution commands:\n" \
    "   run\n" \
    "     Run the entire program to completion.\n" \
    "   step\n" \
    "     Run one instruction.\n" \
    "   next\n" \
    "     Run one instruction, skipping over subroutines.\n" \
    "\nDebug commands:\n" \
    "   dump [start] [end]\n" \
    "     Print the contents of memory from [start] to [end].\n" \
    "   exec [start] [end]\n" \
    "     Sets the pages containing the memory from [start] to [end] as executable.\n" \
    "   unexec [start] [end]\n" \
    "     Sets the pages containing the memory from [start] to [end] as not executable.\n" \
    "   register [name] [value]\n" \
    "     Manually set the value of the given register [name] with [value].\n" \
    "   set [address] [value]\n" \
    "     Manually set the location in memory at [address] with [value].\n"

#ifndef __APPLE__
int snprintf(char *str, size_t size, const char *format, ...);
#endif /* __APPLE__ */

/* Global variables */
const char *const REGISTER_NAMES[] = {
    "r0",
    "r1",
    "r2",
    "r3",
    "r4",
    "r5",
    "r6",
    "r7",
    "r8",
    "r9",
    "r10",
    "r11",
    "rio",
    "rrt",
    "rer",
    "rpc",
};

const char *const ERROR_NAMES[] = {
    "No error",
    "Illegal operation",
    "Arithmetic exception",
    "I/O error",
    "TTY error",
    "Permission denied",
    "Page not executable",
    "Out of memory",
    /* Internal error ommitted, too far away */
};

enum emulator_action {
    RUN,
    STEP,
    NEXT,
    DUMP,
    SET_REGISTER,
    SET_MEMORY,
    SET_MEMORY_EXECUTABLE,
    SET_MEMORY_NOT_EXECUTABLE,
    HELP,
    QUIT,
    INVALID,
    DO_NOTHING,
};

/* Static declarations */
static bool run_next_instruction_or_subroutine(struct program_state *state);
static enum emulator_action parse_emulator_action(struct program_state *state, const char *str);
static void print_program_state(struct program_state *state);
static void print_interpreted_value(struct program_state *state, uint32_t value);
static bool run_interactive_command(struct program_state *state, const char *input, enum emulator_action action);

static bool interactive_dump_memory(struct program_state *state, const char *start_buf, const char *end_buf);
static bool interactive_register_change(struct program_state *state, const char *reg_buf, const char *val_buf);
static bool interactive_memory_change(struct program_state *state, const char *addr_buf, const char *val_buf);
static bool interactive_memory_exec(struct program_state *state, const char *start_buf, const char *end_buf, bool executable);
static int32_t parse_numeric_argument(const char *str, struct parse_status *status);

/* Function implementations */
void run_interactively(struct program_state *state)
{
    /* Print greetings */
    journal("Running interactively, colors are %s.\n",
            state->opt->colors ? "enabled" : "disabled");
    printf("antz assembler and emulator, version %s.\n"
           "For help, type \"help\". To quit, type \"quit\".\n",
            VERSION_STRING);

    /* Set up interactive environment */
    char *input;
    bool print_state = true;
    enum emulator_action action, previous_exec_action = DO_NOTHING;
    /* TODO add proper tab completion */
    rl_bind_key('\t', rl_complete);

    while (true) {
        if (print_state) {
            print_program_state(state);
            print_state = false;
        }

        input = readline("(cmd) ");

        if (!input) {
            /* If input is NULL, it means EOF */
            return;
        } else if (STREMPTY(input)) {
            /* If they just hit enter, run the previous command */
            action = previous_exec_action;
        } else {
            add_history(input);
            journal("User command: \"%s\".\n", input);
            action = parse_emulator_action(state, input);
        }

        switch (action) {
            case DO_NOTHING:
                break;
            case RUN:
                journal("Running program to completion. (interactive mode)\n");
                while (run_next_instruction(state));
                previous_exec_action = RUN;
                print_state = true;
                break;
            case STEP:
                journal("Running one instruction. (interactive mode)\n");
                run_next_instruction(state);
                previous_exec_action = STEP;
                print_state = true;
                break;
            case NEXT:
                journal("Running one instruction or subroutine. (interactive mode)\n");
                run_next_instruction_or_subroutine(state);
                previous_exec_action = NEXT;
                print_state = true;
                break;
            case DUMP:
                journal("Printing memory in range. (interactive mode)\n");
                run_interactive_command(state, input, action);
                break;
            case SET_REGISTER:
                journal("Manually setting register. (interactive mode)\n");
                print_state = run_interactive_command(state, input, action);
                break;
            case SET_MEMORY:
                journal("Manually setting memory address. (interactive mode)\n");
                run_interactive_command(state, input, action);
                break;
            case SET_MEMORY_EXECUTABLE:
                journal("Manually setting the page to be executable. (interactive mode)\n");
                run_interactive_command(state, input, action);
                break;
            case SET_MEMORY_NOT_EXECUTABLE:
                journal("Manually setting the page to be not executable. (interactive mode)\n");
                run_interactive_command(state, input, action);
                break;
            case HELP:
                journal("Printing help string. (interactive mode)\n");
                printf(INTERACTIVE_EMULATOR_HELP_STRING);
                break;
            case QUIT:
                free(input);
                return;
            case INVALID:
                printf("%sUnknown command.%s\n",
                        RED_BOLD,
                        ENDCOLOR);
                free(input);
                continue;
        }

        free(input);
    }
}

bool run_next_instruction(struct program_state *state)
{
    const uint32_t instruction = read_executable_word(state, state->registers[REGISTER_RPC]);

    if (instruction == INSTRUCTION_HALT) {
        return false;
    }

    state->registers[REGISTER_RPC]++;
    decode_and_run(state, instruction);
    return true;
}

static bool run_next_instruction_or_subroutine(struct program_state *state)
{
    uint32_t instruction;
    unsigned int skipped = 0;

    instruction = read_executable_word(state, state->registers[REGISTER_RPC]);

    if (instruction == INSTRUCTION_HALT) {
        return false;
    }

    state->registers[REGISTER_RPC]++;
    decode_and_run(state, instruction);

    switch ((instruction & OPCODE_MASK) >> 26) {
        case OPCODE_CALL:
        case OPCODE_TRAP:
            do {
                instruction = read_executable_word(state, state->registers[REGISTER_RPC]);

                if (instruction == INSTRUCTION_HALT) {
                    return false;
                }

                state->registers[REGISTER_RPC]++;
                decode_and_run(state, instruction);
                skipped++;
            } while (instruction != INSTRUCTION_RET);
            break;
    }

    if (skipped > 0) {
        printf("%u instructions skipped.\n", skipped);
    }

    return true;
}

void set_register(struct program_state *state, reg register_id, int32_t value)
{
    if (register_id == REGISTER_RPC) {
        /* rpc is read only, so set the error flag */
        journal("Attempt by user-space to set rpc.\n");
        state->registers[REGISTER_RER] = RERROR_ILLEGAL_OPERATION;
        state->last_register = REGISTER_RER;
    } else {
        state->registers[register_id] = value;
        state->last_register = register_id;
    }
}

int32_t get_register(struct program_state *state, reg register_id)
{
    return state->registers[register_id];
}

static enum emulator_action parse_emulator_action(struct program_state *state, const char *str)
{
    /* Consume all leading whitespace */
    size_t i;
    for (i = 0; str[i] && isspace(str[i]); i++);

    /* Find the first possible command */
    switch (str[i]) {
        case '\0':
            return DO_NOTHING;
        case 'D':
        case 'd':
            return DUMP;
        case 'E':
        case 'e':
            return SET_MEMORY_EXECUTABLE;
        case 'H':
        case 'h':
            return HELP;
        case 'R':
        case 'r':
            switch (str[i + 1]) {
                case 'U':
                case 'u':
                    return RUN;
                case 'E':
                case 'e':
                    return SET_REGISTER;
                case ' ':
                case '\0':
                    printf("%sAmbigious command.%s\nDid you mean \"run\" or \"register\"?\n",
                            RED_BOLD,
                            ENDCOLOR);
                    return DO_NOTHING;
                default:
                    return INVALID;
            }
        case 'S':
        case 's':
            switch (str[i + 1]) {
                case 'E':
                case 'e':
                    return SET_MEMORY;
                case 'T':
                case 't':
                    return STEP;
                case ' ':
                case '\0':
                    printf("%sAmbigious command.%s\nDid you mean \"set\" or \"step\"?\n",
                            RED_BOLD,
                            ENDCOLOR);
                    return DO_NOTHING;
            }
            return SET_MEMORY;
        case 'N':
        case 'n':
            return NEXT;
        case 'U':
        case 'u':
            return SET_MEMORY_NOT_EXECUTABLE;
        case 'Q':
        case 'q':
            return QUIT;
        default:
            return INVALID;
    }
}

static void print_program_state(struct program_state *state)
{
    putchar('\n');

    /* Print registers */
    uint32_t value;
    reg i;
    for (i = 0; i < 14; i++) {
        value = state->registers[i];
        printf("%s%s%s: %s%s%04x %04x  ",
                WHITE_BOLD,
                get_register_name(i),
                ENDCOLOR,
                (i < 10) ? " " : "",
                (state->opt->colors && state->last_register == i) ? BLUE_ANSI : "",
                value >> 16,
                value & 0xffff);
        print_interpreted_value(state, value);

        if (state->opt->colors && state->last_register == i) {
            printf(ENDCOLOR_ANSI);
        }

        putchar('\t');

        if (i % 3 == 2) {
            putchar('\n');
        }
    }

    /* The error register has different value interpretations */
    value = state->registers[REGISTER_RER];
    printf("%srer%s: %04x %04x  ",
            WHITE_BOLD,
            ENDCOLOR,
            value >> 16,
            value & 0xffff);

    if (value != RERROR_OK) {
        printf("%s%s%s\n",
                RED_BOLD,
                get_error_name(value),
                ENDCOLOR);
    } else {
        printf("0\n");
    }

    /* Print current instruction */
    value = state->registers[REGISTER_RPC];
    printf("\nNow at %s%04x %04x%s: %s",
            GREEN_BOLD,
            value >> 16,
            value & 0xffff,
            ENDCOLOR,
            YELLOW);
    decode_and_print(read_word(state->mem, value));
    printf("%s", ENDCOLOR);
}

static void print_interpreted_value(struct program_state *state, uint32_t value)
{
    if (value < 0xff) {
        switch (value) {
            case '\\':
                printf("%s'\\\\'%s", MAGENTA, ENDCOLOR);
                return;
            case '\'':
                printf("%s'\\\''%s", MAGENTA, ENDCOLOR);
                return;
            case '\n':
                printf("%s'\\n'%s", MAGENTA, ENDCOLOR);
                return;
            case '\r':
                printf("%s'\\r'%s", MAGENTA, ENDCOLOR);
                return;
            case '\t':
                printf("%s'\\t'%s", MAGENTA, ENDCOLOR);
                return;
            case '\b':
                printf("%s'\\b'%s", MAGENTA, ENDCOLOR);
                return;
            case '\f':
                printf("%s'\\f'%s", MAGENTA, ENDCOLOR);
                return;
            case '\v':
                printf("%s'\\v'%s", MAGENTA, ENDCOLOR);
                return;
        }

        if (isprint(value)) {
            printf("%s'%c'%s", MAGENTA, value, ENDCOLOR);
            return;
        }
    }

    /* TODO check for labels */
    UNUSED(state);

    printf("%d", (int32_t)value);
}

static bool run_interactive_command(struct program_state *state, const char *input, enum emulator_action action)
{
    /* Consume zeroth argument */
    for (; *input && !isspace(*input); input++);

    if (*input == '\0') {
        printf("%sNot enough arguments.%s Requires 2, not 0.\n",
                RED_BOLD,
                ENDCOLOR);
        return false;
    }

    /* Consume whitespace */
    for (; *input && isspace(*input); input++);

    if (*input == '\0') {
        printf("%sNot enough arguments.%s Requires 2, not 0.\n",
                RED_BOLD,
                ENDCOLOR);
        return false;
    }

    size_t i;
    char arg1_buf[ARGUMENT_BUFFER_LENGTH];
    /* Copy first argument */
    for (i = 0; isalnum(*input); i++) {
        arg1_buf[i] = *input++;

        if (i >= ARGUMENT_BUFFER_LENGTH) {
            printf("%sFirst argument too long.%s Must be less than %d characters.\n",
                    RED_BOLD,
                    ENDCOLOR,
                    ARGUMENT_BUFFER_LENGTH);
            return false;
        }
    }
    arg1_buf[i] = '\0';

    /* Consume whitespace */
    for (; *input && isspace(*input); input++);

    if (*input == '\0') {
        printf("%sNot enough arguments.%s Requires 2, not 1.\n",
                RED_BOLD,
                ENDCOLOR);
        return false;
    }

    char arg2_buf[ARGUMENT_BUFFER_LENGTH];
    /* Copy second argument */
    for (i = 0; isalnum(*input); i++) {
        arg2_buf[i] = *input++;

        if (i >= ARGUMENT_BUFFER_LENGTH) {
            printf("%sSecond argument too long.%s Must be less than %d characters.\n",
                    RED_BOLD,
                    ENDCOLOR,
                    ARGUMENT_BUFFER_LENGTH);
            return false;
        }
    }
    arg2_buf[i] = '\0';

    switch (action) {
        case DUMP:
            return interactive_dump_memory(state, arg1_buf, arg2_buf);
        case SET_REGISTER:
            return interactive_register_change(state, arg1_buf, arg2_buf);
        case SET_MEMORY:
            return interactive_memory_change(state, arg1_buf, arg2_buf);
        case SET_MEMORY_EXECUTABLE:
            return interactive_memory_exec(state, arg1_buf, arg2_buf, true);
        case SET_MEMORY_NOT_EXECUTABLE:
            return interactive_memory_exec(state, arg1_buf, arg2_buf, false);
        default:
            return false;
    }
}

static bool interactive_dump_memory(struct program_state *state, const char *start_buf, const char *end_buf)
{
    struct parse_status status = {
        .filename = "<console>",
        .lineno = 0,
    };

    uint32_t start_addr = parse_numeric_argument(start_buf, &status);
    if (!status.success) {
        return false;
    }

    uint32_t end_addr = parse_numeric_argument(end_buf, &status);
    if (!status.success) {
        return false;
    }

    if (start_addr >= end_addr) {
        printf("%sInvalid address range.%s\n",
                RED_BOLD,
                ENDCOLOR);
        return false;
    }

    char line[61];
    line[60] = '\0';

    uint32_t addr, word;
    unsigned int column = 0;
    for (addr = start_addr; addr < end_addr; addr++) {
        word = read_word(state->mem, addr);

        /* Print address */
        if (column == 0) {
                printf("%s%04x %04x:%s ",
                        WHITE_BOLD,
                        addr >> 16,
                        addr & 0xffff,
                        ENDCOLOR);
        }

        /* Write hex digits */
        snprintf(line + (10 * column), 11, "%04x %04x ",
                word >> 16,
                word & 0xffff);

        line[44 + 4 * column] = ASCIIFY(word >> 24);
        line[45 + 4 * column] = ASCIIFY((word >> 16) & 0xff);
        line[46 + 4 * column] = ASCIIFY((word >> 8) & 0xff);
        line[47 + 4 * column] = ASCIIFY(word & 0xff);

        /* Print contents of the buffer */
        if (column == 3) {
            line[40] = ' ';
            line[41] = ' ';
            line[42] = ' ';
            line[43] = ' ';
            printf("%s\n", line);

            /* Reset buffer and column */
            size_t i;
            for (i = 0; i < 60; i++) {
                line[i] = ' ';
            }
            column = 0;
        } else {
            column++;
        }
    }

    /* Print the incomplete line */
    if (column > 0) {
        /* Replace nulls with spaces */
        while (column < 4) {
            snprintf(line + (10 * column), 11, "          ");
            column++;
        }

        line[40] = ' ';
        line[41] = ' ';
        line[42] = ' ';
        line[43] = ' ';
        printf("%s\n", line);
    }

    putchar('\n');
    return true;
}

static bool interactive_register_change(struct program_state *state, const char *reg_buf, const char *val_buf)
{
    struct parse_status status = {
        .filename = "<console>",
        .lineno = 0,
    };

    reg reg = parse_register(reg_buf, &status);
    if (!status.success) {
        return false;
    }

    uint32_t value = parse_numeric_argument(val_buf, &status);
    if (!status.success) {
        return false;
    }

    journal("Setting %s to 0x%08x.\n", get_register_name(reg), value);
    set_register(state, reg, value);
    return true;
}

static bool interactive_memory_change(struct program_state *state, const char *addr_buf, const char *val_buf)
{
    struct parse_status status = {
        .filename = "<console>",
        .lineno = 0,
    };

    uint32_t addr = parse_numeric_argument(addr_buf, &status);
    if (!status.success) {
        return false;
    }

    uint32_t value = parse_numeric_argument(val_buf, &status);
    if (!status.success) {
        return false;
    }

    journal("Setting address 0x%08x to 0x%08x.n", addr, value);
    write_word(state->mem, addr, value);
    return true;
}

static bool interactive_memory_exec(struct program_state *state, const char *start_buf, const char *end_buf, bool executable)
{
    struct parse_status status = {
        .filename = "<console>",
        .lineno = 0,
    };

    uint32_t start_addr = parse_numeric_argument(start_buf, &status);
    if (!status.success) {
        return false;
    }

    uint32_t end_addr = parse_numeric_argument(end_buf, &status);
    if (!status.success) {
        return false;
    }

    printf("Setting addresses 0x%08x to 0x%08x to be %sexecutable.\n",
            start_addr,
            end_addr,
            executable ? "" : "not ");
    memory_set_executable(state->mem, start_addr, end_addr, executable);
    return true;
}

static int32_t parse_numeric_argument(const char *str, struct parse_status *status)
{
    if (STREQUALS2(str, "0b", 2) || STREQUALS2(str, "0B", 2)) {
        return parse_binary_number(str, status);
    } else if (STREQUALS2(str, "0o", 2) || STREQUALS2(str, "0O", 2)) {
        return parse_octal_number(str, status);
    } else if (STREQUALS2(str, "0x", 2) || STREQUALS2(str, "0X", 2)) {
        return parse_hex_number(str, status);
    } else {
        return parse_decimal_number(str, status);
    }
}

