/*
 * assembly.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ASSEMBLY_H
#define __ASSEMBLY_H

#include <stdint.h>

#include "emulator.h"
#include "str_hashmap.h"
#include "memory.h"
#include "vector.h"

struct instruction {
    uint32_t address;
    const char *opcode;
    vector arguments;
    bool pseudo;
};

struct preassembly {
    str_hashmap labels;
    vector instructions;
};

struct preassembly *parse_syntax(char *data, size_t length, const char *filename);
struct memory *assemble(char *data, size_t length, const char *filename, str_hashmap *labels);

#define preassembly_add_label(preasm, label, address)   (str_hashmap_put((preasm)->labels, (label), (address))

#endif /* __ASSEMBLY_H */

