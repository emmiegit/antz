/*
 * opcode.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OPCODE_H
#define __OPCODE_H

#include <stdint.h>

#include "arguments.h"
#include "emulator.h"

union float_bytes {
    float value;
    int32_t bytes;
};

/* -1 signifies an aliased op */
#define OPCODE_ADD              000
#define OPCODE_AND              001
#define OPCODE_CALL             076
#define OPCODE_CMP              005
#define OPCODE_DIV              006
#define OPCODE_FADD             060
#define OPCODE_FCMP             064
#define OPCODE_FDIV             061
#define OPCODE_FMUL             062
#define OPCODE_FNEG             063
#define OPCODE_JEZ              051
#define OPCODE_JGE              055
#define OPCODE_JGZ              053
#define OPCODE_JLE              056
#define OPCODE_JLZ              052
#define OPCODE_JMP              050
#define OPCODE_JNZ              054
#define OPCODE_JRO              057
#define OPCODE_LD               030
#define OPCODE_LDB              031
#define OPCODE_LDI              032
#define OPCODE_LDR              033
#define OPCODE_LEA              034
#define OPCODE_LSHF             013
#define OPCODE_MOD              007
#define OPCODE_MOV               -1
#define OPCODE_MOVB             021
#define OPCODE_MUL              010
#define OPCODE_NOP               -1
#define OPCODE_NOT              002
#define OPCODE_OR               003
#define OPCODE_RET               -1
#define OPCODE_RSHF             012
#define OPCODE_SET              011
#define OPCODE_ST               040
#define OPCODE_STB              041
#define OPCODE_STI              042
#define OPCODE_STR              043
#define OPCODE_SUB              004
#define OPCODE_TRAP             077
#define OPCODE_XOR              020

#define OPCODE_MASK             0xfc000000 /* shift 26 times */
#define OPCODE_REST_MASK        0x03ffffff
#define OPCODE_REST_SEXT(x)     (((x) & OPCODE_REST_MASK) | (((x) & 0x02000000) ? 0xfc000000 : 0))

#define REGISTER_1_MASK         0x03c00000 /* shift 22 times */
#define REG_1_REST_MASK         0x003fffff
#define REG_1_REST_SEXT(x)      (((x) & REG_1_REST_MASK) | (((x) & 0x00200000) ? 0xffc00000 : 0))

#define REGISTER_2_MASK         0x003c0000 /* shift 18 times */
#define REG_2_REST_MASK         0x0003ffff
#define REG_2_REST_SEXT(x)      (((x) & REG_2_REST_MASK) | (((x) & 0x00020000) ? 0xfffc0000 : 0))

#define REGISTER_3_MASK         0x0003c000 /* shift 14 times */
#define REG_3_REST_MASK         0x00003fff
#define REG_3_REST_SEXT(x)      (((x) & REG_3_REST_MASK) | (((x) & 0x00002000) ? 0xffffc000 : 0))

/* For flavored 0/1 register instructions like CALL */
#define FLAVOR1_MASK            0x02000000 /* treat as boolean */
#define FLAV1_REG_1_MASK        0x01e00000 /* shift 21 times */
#define FLAV1_VALUE_MASK        0x01ffffff
#define FLAV1_VALUE_SEXT(x)     (((x) & FLAV1_VALUE_MASK) | (((x) & 0x01000000) ? 0xfe000000 : 0))

/* For flavored 1/2 register instructions */
#define FLAVOR2_MASK            0x00200000 /* treat as boolean */
#define FLAV2_REG_2_MASK        0x001e0000 /* shift 17 times */
#define FLAV2_VALUE_MASK        0x001fffff
#define FLAV2_VALUE_SEXT(x)     (((x) & FLAV2_VALUE_MASK) | (((x) & 0x00100000) ? 0xffe00000 : 0))

/* For flavored 2/3 register instructions like ADD */
#define FLAVOR3_MASK            0x00020000 /* treat as boolean */
#define FLAV3_REG_3_MASK        0x0001e000 /* shift 13 times */
#define FLAV3_VALUE_MASK        0x0001ffff
#define FLAV3_VALUE_SEXT(x)     (((x) & FLAV3_VALUE_MASK) | (((x) & 0x00010000) ? 0xfffe0000 : 0))

/* For ldb and stb */
#define TWO_BIT_MASK            0x00300000 /* shift 20 times */
#define TWO_BIT_REST_MASK       0x000fffff
#define TWO_BIT_REST_SEXT(x)    (((x) & TWO_BIT_REST_MASK) | (((x) & 0x00080000) ? 0xfff00000 : 0))

/* For movb */
#define TWO_BIT_MASK2           0x000c0000 /* shift 18 times */
#define TWO_BIT_REST2_MASK      0x0003ffff
#define TWO_BIT_REST2_SEXT(x)   (((x) & TWO_BIT_REST_MASK2) | (((x) & 0x00040000) ? 0xfffc0000 : 0))

/* Pre-calculated instruction values */
#define INSTRUCTION_NOP         0x00000000 /* NOP (aka ADD R0, R0, 0) */
#define INSTRUCTION_RET         0xa1a00000 /* JMP RRT (aka RET) */
#define INSTRUCTION_HALT        0xffffc000 /* TRAP xfff (aka HALT) */

void decode_and_run(struct program_state *state, const uint32_t instruction);
void decode_and_print(const uint32_t instruction);

#endif /* __OPCODE_H */

