/*
 * memory.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MEMORY_H
#define __MEMORY_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "device.h"
#include "emulator.h"
#include "int_hashmap.h"

#define PAGE_SIZE                   0x0400 /* 1K */
#define EXECUTABLE_PAGE_TABLE_START 0x1000
#define EXECUTABLE_PAGE_TABLE_SIZE  0x1000
#define EXECUTABLE_PAGE_TABLE_END   (EXECUTABLE_PAGE_TABLE_START + EXECUTABLE_PAGE_TABLE_SIZE)

struct mem_page {
    int32_t *data;
    unsigned int pageno;
};

struct memory {
    int_hashmap pages;
    int_hashmap mappings;
    struct mem_page *last_page;
};

struct memory *memory_init();
void memory_destroy(struct memory *mem);

bool memory_set_executable(struct memory *mem, uint32_t start_addr, uint32_t end_addr, bool executable);
bool memory_map(struct memory *mem, uint32_t address, struct device_handler *dev);
bool memory_unmap(struct memory *mem, uint32_t address);

uint32_t read_word(struct memory *mem, uint32_t address);
uint32_t read_executable_word(struct program_state *state, uint32_t address);
uint8_t read_byte(struct memory *mem, uint32_t address, unsigned int byte);
void write_word(struct memory *mem, uint32_t address, uint32_t value);
void write_byte(struct memory *mem, uint32_t address, unsigned int byte, uint8_t value);

#endif /* __MEMORY_H */

/* vim: set ft=c: */
