/*
 * arguments.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARGUMENTS_H
#define __ARGUMENTS_H

#include <stdbool.h>

enum run_mode {
    ASSEMBLE_ONLY,
    RUN_ONLY,
    ASSEMBLE_AND_RUN,
};

typedef struct {
    const char *input_file;
    const char *output_file;
    const char *journal_file;
    const char *hdd_file;
    enum run_mode mode;
    bool interactive;
    bool colors;
    bool check_page_mode;
} options;

void parse_arguments(options *opt, int argc, const char *argv[]);

#endif /* __ARGUMENTS_H */

