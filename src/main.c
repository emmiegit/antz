/*
 * main.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "arguments.h"
#include "assembly.h"
#include "char_vector.h"
#include "device.h"
#include "emulator.h"
#include "journal.h"
#include "main.h"
#include "memory.h"
#include "signal_handler.h"
#include "str_hashmap.h"

#define UNUSED(x)       ((void)(x))

/* Static variables */
static options opt;
static str_hashmap labels = NULL;

/* Static function declarations */
static void load_program_from_file(struct program_state *state);
static void do_assembly(struct program_state *state);
static void run_program(struct program_state *state);
static char *read_file(size_t *filesize);
static char *read_file_using_vector(FILE *fh, char *buf, size_t *filesize);

/* Function implementations */
int main(int argc, const char *argv[])
{
    set_up_signal_handlers();
    parse_arguments(&opt, argc, argv);
    open_journal(opt.journal_file);

    struct program_state state = {
        .opt = &opt,
        .last_register = REGISTER_RPC + 1,
        .registers = {
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
        },
    };

    switch (opt.mode) {
        case RUN_ONLY:
            load_program_from_file(&state);
            run_program(&state);
            break;
        case ASSEMBLE_ONLY:
            do_assembly(&state);
            break;
        case ASSEMBLE_AND_RUN:
            do_assembly(&state);
            run_program(&state);
            break;
    }

    cleanup(0);
    return EXIT_SUCCESS;
}

void cleanup(int ret)
{
    if (labels) {
        str_hashmap_destroy(labels);
    }

    close_journal();
    exit(ret);
}

static void load_program_from_file(struct program_state *state)
{
    size_t i, filesize;
    char *filedata = read_file(&filesize);

    for (i = 0; i < filesize; i++) {
        write_byte(state->mem, i / 4, 3 - (i % 4), filedata[i]);
    }
}

static void do_assembly(struct program_state *state)
{
    size_t filesize;
    char *filedata = read_file(&filesize);
    state->mem = assemble(filedata, filesize, opt.input_file, &labels);
    free(filedata);

    if (state->mem == NULL) {
        printf("Assembly failed.\n");
        cleanup(1);
    }
}

static void run_program(struct program_state *state)
{
    /* Set up emulator */
    register_keyboard(state);
    register_monitor(state);

    /* Set program starting location */
    uint32_t starting_address = read_word(state->mem, 0x2000);
    state->registers[REGISTER_RPC] = (starting_address > 0 ? starting_address : 0x2000);

    /* Run instructions until finish */
    if (opt.interactive) {
        run_interactively(state);
    } else {
        init_raw_keyboard();
        while (run_next_instruction(state));
        deinit_raw_keyboard();
    }

    /* Exit */
    cleanup(state->registers[REGISTER_RER]);
}

static char *read_file(size_t *filesize)
{
    struct stat statbuf;
    int ret = stat(opt.input_file, &statbuf);

    if (ret < 0) {
        fprintf(stderr, "Unable to stat file \"%s\": %s\n",
                opt.input_file,
                strerror(errno));
        cleanup(1);
    }

    *filesize = statbuf.st_size;
    char *buffer = malloc(*filesize * sizeof(char));
    if (buffer == NULL) {
        fprintf(stderr, "Unable to allocate memory for file: %s.\n",
                strerror(errno));
        free(buffer);
        cleanup(1);
    }

    FILE *fh = fopen(opt.input_file, "r");
    if (fh == NULL) {
        fprintf(stderr, "Unable to open file \"%s\": %s.\n",
                opt.input_file,
                strerror(errno));
        free(buffer);
        cleanup(1);
    }

    size_t i;
    int ch;
    for (i = 0; (ch = getc(fh)) != EOF; i++) {
        if (i >= *filesize) {
            /* The stat was misleading, dynamically allocate a buffer */
            return read_file_using_vector(fh, buffer, filesize);
        }

        buffer[i] = (char)ch;
    }

    ret = fclose(fh);
    if (ret == EOF) {
        fprintf(stderr, "Unable to close file \"%s\": %s\n",
                opt.input_file,
                strerror(errno));
    }

    return buffer;
}

static char *read_file_using_vector(FILE *fh, char *buf, size_t *filesize)
{
    char_vector v = char_vector_init_c(*filesize);
    if (v == NULL) {
        fprintf(stderr, "Unable to allocate char_vector: %s.\n",
                strerror(ENOMEM));
        free(buf);
        fclose(fh);
        cleanup(1);
    }

    size_t i;
    for (i = 0; i < *filesize; i++) {
        if (!char_vector_append(v, buf[i])) {
            fprintf(stderr, "Unable to append to char_vector: %s.\n",
                    strerror(ENOMEM));
            free(buf);
            fclose(fh);
            cleanup(1);
        }
    }
    free(buf);

    int ch;
    while ((ch = getc(fh)) != EOF) {
        if (!char_vector_append(v, (char)ch)) {
            fprintf(stderr, "Unable to append to char_vector: %s.\n",
                    strerror(ENOMEM));
            fclose(fh);
            cleanup(1);
        }
    }

    fclose(fh);

    buf = char_vector_to_buffer(v);
    char_vector_destroy(v);
    return buf;
}

