/*
 * assembly_util.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ASSEMBLY_UTIL_H
#define __ASSEMBLY_UTIL_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "emulator.h"

struct parse_status {
    const char *filename;
    size_t lineno;
    uint32_t address;
    bool success;
};

reg parse_register(const char *str, struct parse_status *status);
int32_t parse_binary_number(const char *str, struct parse_status *status);
int32_t parse_octal_number(const char *str, struct parse_status *status);
int32_t parse_decimal_number(const char *str, struct parse_status *status);
int32_t parse_hex_number(const char *str, struct parse_status *status);
char parse_character_literal(const char *str, struct parse_status *status);
char *parse_string_literal(const char *str, size_t *length, struct parse_status *status);

int print_parse_message(const struct parse_status *status, const char *format, ...);
void print_bad_line_snippet(const char *line, size_t start, size_t length);

bool string_equals_ignore_case(const char *a, const char *b);
#endif /* __ASSEMBLY_UTIL_H */

