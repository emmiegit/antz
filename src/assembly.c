/*
 * assembly.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "assembly.h"
#include "assembly_util.h"
#include "char_vector.h"
#include "main.h"
#include "memory.h"
#include "parser.h"
#include "journal.h"
#include "vector.h"

#define STREQUALS(x, y)         (strcmp((x), (y)) == 0)
#define ISIDENT(x)              (isalpha(x) || isdigit(x) || ((x) == '_'))
#define UNUSED(x)               ((void)(x))

static struct preassembly *preassembly_init();
static void preassembly_destroy(struct preassembly *preasm);

struct preassembly *parse_syntax(char *data, size_t length, const char *filename)
{
    struct parse_status status = {
        .filename = filename,
        .lineno = 0,
        .success = true,
    };

    struct preassembly *preasm = preassembly_init();
    if (preasm == NULL) {
        fprintf(stderr, "Unable to allocate preassembly structure.\n");
        return NULL;
    }


    /*
    yyscan_t scanner;

    if (yylex_init(&scanner)) {
        return NULL;
    }

    state = yy_scan_string(expr, scanner);

    if (yyparse(&expression, scanner)) {
        return NULL;
    }

    yy_delete_buffer(state, scanner);
    yylex_destroy(scanner);

    */

    return preasm;
}

struct memory *assemble(char *data, size_t length, const char *filename, str_hashmap *labels)
{
    struct preassembly *preasm = parse_syntax(data, length, filename);
    if (preasm == NULL) {
        return NULL;
    }

    struct memory *mem = memory_init();
    if (mem == NULL) {
        fprintf(stderr, "Unable to allocate program virtual memory: %s\n",
                strerror(errno));
        return NULL;
    }

    *labels = preasm->labels;
    preassembly_destroy(preasm);
    return mem;
}

static struct preassembly *preassembly_init()
{
    struct preassembly *preasm = malloc(sizeof(struct preassembly));
    if (preasm == NULL) {
        fprintf(stderr, "Unable to allocate pre-assembly data block: %s\n",
                strerror(errno));
        return NULL;
    }

    preasm->instructions = vector_init();
    if (preasm->instructions == NULL) {
        return NULL;
    }

    preasm->labels = str_hashmap_init();
    if (preasm->labels == NULL) {
        return NULL;
    }

    return preasm;
}

static void preassembly_destroy(struct preassembly *preasm)
{
    struct instruction *instr;
    size_t i, j;

    /* Free all instruction structs */
    for (i = 0; i < preasm->instructions->length; i++) {
        instr = preasm->instructions->array[i];

        /* Free all arguments */
        for (j = 0; j < instr->arguments->length; j++) {
            free(instr->arguments->array[j]);
        }

        free((char *)instr->opcode);
        vector_destroy(instr->arguments);
        free(instr);
    }

    /* Call destructors */
    vector_destroy(preasm->instructions);
    free(preasm);
}

