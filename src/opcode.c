/*
 * opcode.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "emulator.h"
#include "memory.h"
#include "journal.h"
#include "opcode.h"

/* Too many static function declarations, moved them to another file */
#include "opcode_static.h"

#define UNUSED(x)           ((void)(x))

/* Running instructions */
void decode_and_run(struct program_state *state, const uint32_t instruction) {
    const unsigned int opcode = (instruction & OPCODE_MASK) >> 26;

    switch (opcode) {
        case OPCODE_ADD:
            run_add(state, instruction);
            break;
        case OPCODE_AND:
            run_and(state, instruction);
            break;
        case OPCODE_CALL:
            run_call(state, instruction);
            break;
        case OPCODE_CMP:
            run_cmp(state, instruction);
            break;
        case OPCODE_DIV:
            run_div(state, instruction);
            break;
        case OPCODE_FADD:
            run_fadd(state, instruction);
            break;
        case OPCODE_FCMP:
            run_fcmp(state, instruction);
            break;
        case OPCODE_FDIV:
            run_fdiv(state, instruction);
            break;
        case OPCODE_FMUL:
            run_fmul(state, instruction);
            break;
        case OPCODE_FNEG:
            run_fneg(state, instruction);
            break;
        case OPCODE_JEZ:
            run_jez(state, instruction);
            break;
        case OPCODE_JGE:
            run_jge(state, instruction);
            break;
        case OPCODE_JGZ:
            run_jgz(state, instruction);
            break;
        case OPCODE_JLE:
            run_jle(state, instruction);
            break;
        case OPCODE_JLZ:
            run_jlz(state, instruction);
            break;
        case OPCODE_JMP:
            run_jmp(state, instruction);
            break;
        case OPCODE_JNZ:
            run_jnz(state, instruction);
            break;
        case OPCODE_JRO:
            run_jro(state, instruction);
            break;
        case OPCODE_LD:
            run_ld(state, instruction);
            break;
        case OPCODE_LDB:
            run_ldb(state, instruction);
            break;
        case OPCODE_LDI:
            run_ldi(state, instruction);
            break;
        case OPCODE_LDR:
            run_ldr(state, instruction);
            break;
        case OPCODE_LEA:
            run_lea(state, instruction);
            break;
        case OPCODE_LSHF:
            run_lshf(state, instruction);
            break;
        case OPCODE_MOD:
            run_mod(state, instruction);
            break;
        case OPCODE_MOVB:
            run_movb(state, instruction);
            break;
        case OPCODE_MUL:
            run_mul(state, instruction);
            break;
        case OPCODE_NOT:
            run_not(state, instruction);
            break;
        case OPCODE_OR:
            run_or(state, instruction);
            break;
        case OPCODE_RSHF:
            run_rshf(state, instruction);
            break;
        case OPCODE_SET:
            run_set(state, instruction);
            break;
        case OPCODE_ST:
            run_st(state, instruction);
            break;
        case OPCODE_STB:
            run_stb(state, instruction);
            break;
        case OPCODE_STI:
            run_sti(state, instruction);
            break;
        case OPCODE_STR:
            run_str(state, instruction);
            break;
        case OPCODE_SUB:
            run_sub(state, instruction);
            break;
        case OPCODE_TRAP:
            run_trap(state, instruction);
            break;
        case OPCODE_XOR:
            run_xor(state, instruction);
            break;
        default:
            journal("Illegal operation specified: opcode %u.\n", opcode);
            state->registers[REGISTER_RER] = RERROR_ILLEGAL_OPERATION;
    }
}

static void run_add(struct program_state *state, const uint32_t instruction)
{
    if (instruction == INSTRUCTION_NOP) {
        journal("Opcode: nop\n");
        return;
    }

    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result += get_register(state, reg_b);
        journal("Opcode: add: reg %u <- reg %u + reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result += value;
        journal("Opcode: add: reg %u <- reg %u + %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_and(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result &= get_register(state, reg_b);
        journal("Opcode: and: reg %u <- reg %u & reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result &= value;
        journal("Opcode: and: reg %u <- reg %u & %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_call(struct program_state *state, const uint32_t instruction)
{
    journal("Opcode: call: ");
    state->registers[REGISTER_RRT] = state->registers[REGISTER_RPC];
    run_jmp(state, instruction);
}

static void run_cmp(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    const int32_t a = get_register(state, reg_a);
    const int32_t b = get_register(state, reg_b);

    int32_t result;

    if (a < b) {
        result = -1;
    } else if (a == b) {
        result = 0;
    } else {
        /* a > b */
        result = 1;
    }

    journal("Opcode: cmp: reg %u <- cmp(reg %u, reg %u)\n", dest_reg, reg_a, reg_b);
    set_register(state, dest_reg, result);
}

static void run_div(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    journal("Opcode: div: reg %u <- reg %u / reg %u", dest_reg, reg_a, reg_b);

    const int32_t a = get_register(state, reg_a);
    const int32_t b = get_register(state, reg_b);

    if (b == 0) {
        set_register(state, REGISTER_RER, RERROR_ARITHMETIC);
        journal(" - div by zero!\n");
    } else {
        set_register(state, dest_reg, a / b);
        journal("\n");
    }
}

static void run_fadd(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    journal("Opcode: fadd: reg %u <- reg %u + reg %u\n", dest_reg, reg_a, reg_b);

    union float_bytes a, b, result;
    a.bytes = get_register(state, reg_a);
    b.bytes = get_register(state, reg_b);
    result.value = a.value + b.value;

    set_register(state, dest_reg, result.bytes);
}

static void run_fcmp(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    journal("Opcode: fcmp: reg %u <- fcmp(reg %u, reg %u)\n", dest_reg, reg_a, reg_b);

    union float_bytes a, b;
    int32_t result;
    a.bytes = get_register(state, reg_a);
    b.bytes = get_register(state, reg_b);

    if (a.value < b.value) {
        result = -1;
    } else if (a.value == b.value) {
        result = 0;
    } else {
        /* a.value > b.value */
        result = 1;
    }

    set_register(state, dest_reg, result);
}

static void run_fdiv(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    journal("Opcode: fdiv: reg %u <- reg %u / reg %u\n", dest_reg, reg_a, reg_b);

    union float_bytes a, b, result;
    a.bytes = get_register(state, reg_a);
    b.bytes = get_register(state, reg_b);
    result.value = a.value / b.value;

    set_register(state, dest_reg, result.bytes);
}

static void run_fmul(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    journal("Opcode: fmul: reg %u <- reg %u * reg %u\n", dest_reg, reg_a, reg_b);

    union float_bytes a, b, result;
    a.bytes = get_register(state, reg_a);
    b.bytes = get_register(state, reg_b);
    result.value = a.value * b.value;

    set_register(state, dest_reg, result.bytes);
}

static void run_fneg(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg      = (instruction & REGISTER_2_MASK) >> 18;

    journal("Opcode: fneg: reg %u <- -(reg %u)\n", dest_reg, reg);

    union float_bytes a;
    a.bytes = get_register(state, reg);
    a.value = -a.value;

    set_register(state, dest_reg, a.bytes);
}

static void run_jez(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jez: if(reg %u == 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) == 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jge(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jge: if (reg %u >= 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) >= 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jgz(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jgz: if (reg %u > 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) > 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jle(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jle: if (reg %u <= 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) <= 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jlz(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jlz: if (reg %u < 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) < 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jmp(struct program_state *state, const uint32_t instruction)
{
    if (instruction & FLAVOR1_MASK) {
        /* Use register */
        const int reg = (instruction & FLAV1_REG_1_MASK) >> 21;
        state->registers[REGISTER_RPC] = get_register(state, reg);
        journal("Opcode: jmp: goto reg %u\n", reg);
    } else {
        /* Use offset value */
        const int offset = FLAV1_VALUE_SEXT(instruction);
        state->registers[REGISTER_RPC] += offset;
        journal("Opcode: jmp: goto offset %d\n", offset);
    }
}

static void run_jnz(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: jnz: if (reg %u != 0) goto offset %d\n", reg, offset);

    if (get_register(state, reg) != 0) {
        state->registers[REGISTER_RPC] += offset;
    }
}

static void run_jro(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int32_t offset = get_register(state, reg);

    journal("Opcode: jro %u: goto offset %d\n", reg, offset);

    state->registers[REGISTER_RPC] += offset;
}

static void run_ld(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: ld: reg %u <- mem[offset %d]\n", reg, offset);

    const uint32_t word = read_word(state->mem, state->registers[REGISTER_RPC] + offset);
    set_register(state, reg, word);
}

static void run_ldb(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const unsigned int byteno = (instruction & TWO_BIT_MASK) >> 20;
    const int offset = TWO_BIT_REST_SEXT(instruction);

    journal("Opcode: ldb: reg %u <- byte %d of mem[offset %d]\n", reg, byteno, offset);

    const uint8_t byte = read_byte(state->mem, state->registers[REGISTER_RPC] + offset, byteno);
    set_register(state, reg, byte);
}

static void run_ldi(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: ldi: reg %u <- mem[mem[offset %d]]\n", reg, offset);

    const uint32_t address = read_word(state->mem, state->registers[REGISTER_RPC] + offset);
    const uint32_t word = read_word(state->mem, address);
    set_register(state, reg, word);
}

static void run_ldr(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg addr_reg = (instruction & REGISTER_2_MASK) >> 18;
    const int offset = REG_2_REST_SEXT(instruction);

    journal("Opcode: ldr: reg %u <- mem[reg %u + offset %d]\n", dest_reg, addr_reg, offset);

    const int32_t address = get_register(state, addr_reg);
    const uint32_t word = read_word(state->mem, address + offset);

    set_register(state, dest_reg, word);
}

static void run_lea(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 26;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: lea: reg %u <- offset %d\n", reg, offset);

    set_register(state, reg, state->registers[REGISTER_RPC] + offset);
}

static void run_lshf(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result <<= get_register(state, reg_b);
        journal("Opcode: lshf: reg %u <- reg %u << reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result <<= value;
        journal("Opcode: lshf: reg %u <- reg %u << %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_mod(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    const int32_t a = get_register(state, reg_a);
    const int32_t b = get_register(state, reg_b);

    journal("Opcode: mod: reg %u <- reg %u %% reg %u", dest_reg, reg_a, reg_b);

    if (b == 0) {
        set_register(state, REGISTER_RER, RERROR_ARITHMETIC);
        journal(" - div by zero!\n");
    } else {
        set_register(state, dest_reg, a % b);
        journal("\n");
    }
}

static void run_movb(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg src_reg  = (instruction & REGISTER_2_MASK) >> 18;
    const unsigned int dest_byte = (instruction & TWO_BIT_MASK) >> 20;
    const unsigned int src_byte  = (instruction & TWO_BIT_MASK2) >> 18;

    journal("Opcode: movb: byte %u of reg %u <- byte %u of reg %u", dest_byte, dest_reg, src_byte, src_reg);

    const uint8_t value = get_register(state, src_reg) >> (8 * src_byte);
    set_register(state, dest_reg, value << (8 * dest_byte));
}

static void run_mul(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    const uint32_t a = get_register(state, reg_a);
    const uint32_t b = get_register(state, reg_b);

    journal("Opcode: mul: reg %u <- reg %u * reg %u\n", dest_reg, reg_a, reg_b);

    set_register(state, dest_reg, a * b);
}

static void run_not(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg src_reg  = (instruction & REGISTER_2_MASK) >> 18;

    const uint32_t a = get_register(state, src_reg);

    journal("Opcode: not: reg %u <- ~(reg %u)\n", dest_reg, src_reg);

    set_register(state, dest_reg, ~a);
}

static void run_or(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result |= get_register(state, reg_b);
        journal("Opcode: or: reg %u <- reg %u | reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result |= value;
        journal("Opcode: or: reg %u <- reg %u | %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_rshf(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result >>= get_register(state, reg_b);
        journal("Opcode: rshf: reg %u <- reg %u >> reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = ((instruction & FLAV3_VALUE_MASK) << 21) >> 21;
        result >>= value;
        journal("Opcode: rshf: reg %u <- reg %u >> %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_set(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const int value = REG_1_REST_SEXT(instruction);

    journal("Opcode: set: reg %u <- %d\n", dest_reg, value);
    set_register(state, dest_reg, value);
}

static void run_st(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: st: mem[offset %d] <- reg %u\n", offset, reg);

    const uint32_t word = get_register(state, reg);
    write_word(state->mem, state->registers[REGISTER_RPC] + offset, word);
}

static void run_stb(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const unsigned int byteno = (instruction & TWO_BIT_MASK) >> 20;
    const int offset = TWO_BIT_REST_SEXT(instruction);

    journal("Opcode: stb: mem[offset %d] <- byte %d reg %u\n", offset, byteno, reg);

    const uint8_t byte = get_register(state, reg) & 0xff;
    write_byte(state->mem, state->registers[REGISTER_RPC] + offset, byteno, byte);
}

static void run_sti(struct program_state *state, const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = REG_1_REST_SEXT(instruction);

    journal("Opcode: sti: mem[mem[offset %d]] <- reg %u\n", offset, reg);

    const uint32_t word = get_register(state, reg);
    const uint32_t address = read_word(state->mem, state->registers[REGISTER_RPC] + offset);
    write_word(state->mem, address, word);
}

static void run_str(struct program_state *state, const uint32_t instruction)
{
    const reg src_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg addr_reg = (instruction & REGISTER_2_MASK) >> 18;
    const int offset = REG_2_REST_SEXT(instruction);

    journal("Opcode: mem[reg %u + %d] <- reg %u\n", src_reg, offset, addr_reg);

    const uint32_t word = get_register(state, src_reg);
    const int32_t address = get_register(state, addr_reg);

    write_word(state->mem, address + offset, word);
}

static void run_sub(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result -= get_register(state, reg_b);
        journal("Opcode: sub: reg %u <- reg %u - reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result -= value;
        journal("Opcode: sub: reg %u <- reg %u - %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

static void run_trap(struct program_state *state, const uint32_t instruction)
{
    const unsigned int call_id = (instruction & OPCODE_REST_MASK) >> 14;
    journal("Opcode: trap 0x%012x: rrt <- rpc; rpc <- mem[0x%012x]\n", call_id, call_id);

    state->registers[REGISTER_RRT] = state->registers[REGISTER_RPC];
    state->registers[REGISTER_RPC] = read_word(state->mem, call_id);
}

static void run_xor(struct program_state *state, const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    int32_t result = get_register(state, reg_a);

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        result ^= get_register(state, reg_b);
        journal("Opcode: xor: reg %u <- reg %u ^ reg %u\n", dest_reg, reg_a, reg_b);
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        result ^= value;
        journal("Opcode: xor: reg %u <- reg %u ^ %d\n", dest_reg, reg_a, value);
    }

    set_register(state, dest_reg, result);
}

/* Printing instructions */
void decode_and_print(const uint32_t instruction) {
    const unsigned int opcode = (instruction & OPCODE_MASK) >> 26;

    switch (opcode) {
        case OPCODE_ADD:
            print_add(instruction);
            break;
        case OPCODE_AND:
            print_and(instruction);
            break;
        case OPCODE_CALL:
            print_call(instruction);
            break;
        case OPCODE_CMP:
            print_cmp(instruction);
            break;
        case OPCODE_DIV:
            print_div(instruction);
            break;
        case OPCODE_FADD:
            print_fadd(instruction);
            break;
        case OPCODE_FCMP:
            print_fcmp(instruction);
            break;
        case OPCODE_FDIV:
            print_fdiv(instruction);
            break;
        case OPCODE_FMUL:
            print_fmul(instruction);
            break;
        case OPCODE_FNEG:
            print_fneg(instruction);
            break;
        case OPCODE_JEZ:
            print_jez(instruction);
            break;
        case OPCODE_JGE:
            print_jge(instruction);
            break;
        case OPCODE_JGZ:
            print_jgz(instruction);
            break;
        case OPCODE_JLE:
            print_jle(instruction);
            break;
        case OPCODE_JLZ:
            print_jlz(instruction);
            break;
        case OPCODE_JMP:
            print_jmp(instruction);
            break;
        case OPCODE_JNZ:
            print_jnz(instruction);
            break;
        case OPCODE_JRO:
            print_jro(instruction);
            break;
        case OPCODE_LD:
            print_ld(instruction);
            break;
        case OPCODE_LDB:
            print_ldb(instruction);
            break;
        case OPCODE_LDI:
            print_ldi(instruction);
            break;
        case OPCODE_LDR:
            print_ldr(instruction);
            break;
        case OPCODE_LEA:
            print_lea(instruction);
            break;
        case OPCODE_LSHF:
            print_lshf(instruction);
            break;
        case OPCODE_MOD:
            print_mod(instruction);
            break;
        case OPCODE_MOVB:
            print_movb(instruction);
            break;
        case OPCODE_MUL:
            print_mul(instruction);
            break;
        case OPCODE_NOT:
            print_not(instruction);
            break;
        case OPCODE_OR:
            print_or(instruction);
            break;
        case OPCODE_RSHF:
            print_rshf(instruction);
            break;
        case OPCODE_SET:
            print_set(instruction);
            break;
        case OPCODE_ST:
            print_st(instruction);
            break;
        case OPCODE_STB:
            print_stb(instruction);
            break;
        case OPCODE_STI:
            print_sti(instruction);
            break;
        case OPCODE_STR:
            print_str(instruction);
            break;
        case OPCODE_SUB:
            print_sub(instruction);
            break;
        case OPCODE_TRAP:
            print_trap(instruction);
            break;
        case OPCODE_XOR:
            print_xor(instruction);
            break;
        default:
            journal("Illegal operation specified: opcode %o.\n", opcode);
            printf("??? (literally %08x)\n", instruction);
    }
}

static void print_add(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s + %s\n",
                get_register_name(dest_reg),
                get_register_name(reg_a),
                get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        if (value) {
            printf("%s <- %s + %d\n",
                    get_register_name(dest_reg),
                    get_register_name(reg_a),
                    value);
        } else if (dest_reg == reg_a) {
            printf("nop\n");
        } else {
            printf("%s <- %s\n",
                    get_register_name(dest_reg),
                    get_register_name(reg_a));
        }
    }
}

static void print_and(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s & %s\n",
                get_register_name(dest_reg),
                get_register_name(reg_a),
                get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s & %d\n",
                get_register_name(dest_reg),
                get_register_name(reg_a),
                value);
    }
}

static void print_call(const uint32_t instruction)
{
    if (instruction & FLAVOR1_MASK) {
        /* Use register */
        const int reg = (instruction & FLAV1_REG_1_MASK) >> 21;
        printf("call %s\n", get_register_name(reg));
    } else {
        /* Use offset value */
        const int offset = FLAV1_VALUE_SEXT(instruction);
        printf("call offset %d\n", offset);
    }
}

static void print_cmp(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("%s <- cmp(%s, %s)\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_div(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("%s <- %s / %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_fadd(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("float %s <- %s + %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_fcmp(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("float %s <- cmp(%s, %s)\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_fdiv(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("float %s <- %s / %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_fmul(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("float %s <- %s * %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_fneg(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg      = (instruction & REGISTER_2_MASK) >> 18;

    printf("float %s <- -%s\n",
            get_register_name(dest_reg),
            get_register_name(reg));
}

static void print_jez(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("if (%s == 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jge(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("if (%s >= 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jgz(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("if (%s > 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jle(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("if (%s <= 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jlz(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("if (%s < 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jmp(const uint32_t instruction)
{
    if (instruction & FLAVOR1_MASK) {
        /* Use register */
        const int reg = (instruction & FLAV1_REG_1_MASK) >> 21;
        printf("jmp %s\n", get_register_name(reg));
    } else {
        /* Use offset value */
        const int offset = FLAV1_VALUE_SEXT(instruction);
        printf("jmp offset %d\n", offset);
    }
}

static void print_jnz(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = ((instruction & REG_1_REST_MASK) << 17) >> 17;

    printf("if (%s != 0) goto offset %d\n",
            get_register_name(reg),
            offset);
}

static void print_jro(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;

    printf("jmp offset in %s\n",
            get_register_name(reg));
}

static void print_ld(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = ((instruction & REG_1_REST_MASK) << 17) >> 17;

    printf("%s <- mem[rpc + %d]\n",
            get_register_name(reg),
            offset);
}

static void print_ldb(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const unsigned int byteno = (instruction & TWO_BIT_MASK) >> 20;
    const int offset = ((instruction & TWO_BIT_REST_MASK) << 12) >> 12;

    printf("%s <- byte %u of mem[rpc + %d]\n",
            get_register_name(reg),
            byteno,
            offset);
}

static void print_ldi(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = ((instruction & REG_1_REST_MASK) << 17) >> 17;

    printf("%s <- mem[mem[rpc + %d]]\n",
            get_register_name(reg),
            offset);
}

static void print_ldr(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg addr_reg = (instruction & REGISTER_2_MASK) >> 18;
    const int offset = (instruction & REG_2_REST_MASK);

    printf("%s <- mem[%s + %d]\n",
            get_register_name(dest_reg),
            get_register_name(addr_reg),
            offset);
}

static void print_lea(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 26;
    const int offset = (instruction & REG_1_REST_MASK);

    printf("%s <- rpc + %d\n",
            get_register_name(reg),
            offset);
}

static void print_lshf(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s << %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s << %d\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            value);
    }
}

static void print_mod(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("%s <- %s %% %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_movb(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg src_reg  = (instruction & REGISTER_2_MASK) >> 18;
    const unsigned int dest_byte = (instruction & TWO_BIT_MASK) >> 20;
    const unsigned int src_byte  = (instruction & TWO_BIT_MASK2) >> 18;

    printf("byte %u of %s <- byte %u of %s\n",
            dest_byte,
            get_register_name(dest_reg),
            src_byte,
            get_register_name(src_reg));
}

static void print_mul(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;
    const reg reg_b    = (instruction & REGISTER_3_MASK) >> 14;

    printf("%s <- %s * %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
}

static void print_not(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg src_reg  = (instruction & REGISTER_2_MASK) >> 18;

    printf("%s <- ~%s\n",
            get_register_name(dest_reg),
            get_register_name(src_reg));
}

static void print_or(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s | %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s | %d\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            value);
    }
}

static void print_rshf(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s >> %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s << %d\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            value);
    }
}

static void print_set(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const int value = (instruction & REG_1_REST_MASK);
    printf("%s <- %d\n",
            get_register_name(dest_reg),
            value);
}

static void print_st(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);
    printf("mem[rpc + %d] <- %s\n",
            offset,
            get_register_name(reg));
}

static void print_stb(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const unsigned int byteno = (instruction & TWO_BIT_MASK) >> 20;
    const int offset = (instruction & TWO_BIT_REST_MASK);
    printf("byte %u of mem[rpc + %d] <- %s\n",
            byteno,
            offset,
            get_register_name(reg));
}

static void print_sti(const uint32_t instruction)
{
    const reg reg = (instruction & REGISTER_1_MASK) >> 22;
    const int offset = (instruction & REG_1_REST_MASK);
    printf("mem[mem[rpc + %d]] <- %s\n",
            offset,
            get_register_name(reg));
}

static void print_str(const uint32_t instruction)
{
    const reg src_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg addr_reg = (instruction & REGISTER_2_MASK) >> 18;
    const int offset = (instruction & REG_2_REST_MASK);
    printf("mem[%s + %d] <- %s\n",
            get_register_name(addr_reg),
            offset,
            get_register_name(src_reg));
}

static void print_sub(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s - %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s - %d\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            value);
    }
}

static void print_trap(const uint32_t instruction)
{
    const unsigned int call_id = (instruction & OPCODE_REST_MASK) >> 14;

    switch (call_id) {
        case 0x010:
            printf("getc");
            break;
        case 0x011:
            printf("putc");
            break;
        case 0x012:
            printf("mvcur");
            break;
        case 0x013:
            printf("clscr");
            break;
        case 0x021:
            printf("puts");
            break;
        case 0xfff:
            printf("halt");
            break;
        default:
            printf("???");
    }

    printf(" - trap %03x\n", call_id);
}

static void print_xor(const uint32_t instruction)
{
    const reg dest_reg = (instruction & REGISTER_1_MASK) >> 22;
    const reg reg_a    = (instruction & REGISTER_2_MASK) >> 18;

    if (instruction & FLAVOR3_MASK) {
        /* Two registers */
        const reg reg_b = (instruction & FLAV3_REG_3_MASK) >> 14;
        printf("%s <- %s ^ %s\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            get_register_name(reg_b));
    } else {
        /* One register, one value */
        const int value = FLAV3_VALUE_SEXT(instruction);
        printf("%s <- %s ^ %d\n",
            get_register_name(dest_reg),
            get_register_name(reg_a),
            value);
    }
}

