/*
 * arguments.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "arguments.h"
#include "info.h"
#include "main.h"

#define STREQUALS(x, y) (strcmp((x), (y)) == 0)

#ifndef STDOUT_FILENO
# define STDOUT_FILENO 1
#endif /* STDOUT_FILENO */

static bool set_short_arguments(options *opt, int argc, const char *argv[], int argno);
static void help_and_exit(const char *program_name);
static void usage_and_exit(const char *program_name);
static void version_and_exit();

void parse_arguments(options *opt, int argc, const char *argv[])
{
    if (argc == 1) {
        usage_and_exit(argv[0]);
    }

    opt->mode = ASSEMBLE_AND_RUN;
    opt->input_file = argv[argc - 1];
    opt->journal_file = "/dev/null";
    opt->interactive = false;
    opt->colors = isatty(STDOUT_FILENO);
    opt->check_page_mode = true;

    if (STREQUALS(argv[argc - 1], "--help")) {
        help_and_exit(argv[0]);
    } else if (STREQUALS(argv[argc - 1], "--version")) {
        version_and_exit(argv[0]);
    } else if (STREQUALS(opt->input_file, "-")) {
        opt->input_file = "/dev/stdin";
    }

    int i;
    for (i = 1; i < argc - 1; i++) {
        if (STREQUALS(argv[i], "--assemble")) {
            opt->mode = ASSEMBLE_ONLY;
        } else if (STREQUALS(argv[i], "--run")) {
            opt->mode = RUN_ONLY;
        } else if (STREQUALS(argv[i], "--")) {
            break;
        } else if (STREQUALS(argv[i], "--interactive")) {
            opt->interactive = true;
        } else if (STREQUALS(argv[i], "--no-colors")) {
            opt->colors = false;
        } else if (STREQUALS(argv[i], "--force")) {
            opt->check_page_mode = false;
        } else if (STREQUALS(argv[i], "--journal")) {
            i++;
            if (i >= argc) {
                fprintf(stderr, "\"--journal\" requires an argument.\n");
                free(opt);
                usage_and_exit(argv[0]);
            } else if (i == argc - 1) {
                fprintf(stderr, "Missing input file.\n");
                free(opt);
                usage_and_exit(argv[0]);
            }

            opt->journal_file = argv[i];
        } else if (STREQUALS(argv[i], "--output")) {
            if (i >= argc) {
                fprintf(stderr, "\"--output\" requires an argument.\n");
                free(opt);
                usage_and_exit(argv[0]);
            } else if (i == argc - 1) {
                fprintf(stderr, "Missing input file.\n");
                free(opt);
                usage_and_exit(argv[0]);
            }

            opt->output_file = argv[i];
        } else if (STREQUALS(argv[i], "--help")) {
            free(opt);
            help_and_exit(argv[0]);
        } else if (STREQUALS(argv[i], "-")) {
            free(opt);
            usage_and_exit(argv[0]);
        } else if (argv[i][0] == '-') {
            if (set_short_arguments(opt, argc, argv, i)) {
                i++;
            }
        } else {
            free(opt);
            usage_and_exit(argv[0]);
        }
    }
}

static bool set_short_arguments(options *opt, int argc, const char *argv[], int argno)
{
    bool ret = false;
    size_t i, len = strlen(argv[argno]);
    for (i = 1; i < len; i++) {
        switch (argv[argno][i]) {
            case 'a':
                opt->mode = ASSEMBLE_ONLY;
                break;
            case 'r':
                opt->mode = RUN_ONLY;
                break;
            case 'i':
                opt->interactive = true;
                break;
            case 'n':
                opt->colors = false;
                break;
            case 'f':
                opt->check_page_mode = false;
                break;
            case 'd':
                if (argno + 1 >= argc) {
                    fprintf(stderr, "\"-d\" requires an argument.\n");
                    usage_and_exit(argv[0]);
                } else if (argno + 1 == argc - 1) {
                    fprintf(stderr, "Missing input file.\n");
                    usage_and_exit(argv[0]);
                }

                ret = true;
                opt->hdd_file = argv[argno + 1];
                if (STREQUALS(opt->hdd_file, "-")) {
                    opt->hdd_file = "/dev/stdin";
                }
                break;
            case 'j':
                if (argno + 1 >= argc) {
                    fprintf(stderr, "\"-j\" requires an argument.\n");
                    usage_and_exit(argv[0]);
                } else if (argno + 1 == argc - 1) {
                    fprintf(stderr, "Missing input file.\n");
                    usage_and_exit(argv[0]);
                }

                ret = true;
                opt->journal_file = argv[argno + 1];
                if (STREQUALS(opt->journal_file, "-")) {
                    opt->journal_file = "/dev/stdin";
                }
                break;
            case 'o':
                if (argno + 1 >= argc) {
                    fprintf(stderr, "\"-o\" requires an argument.\n");
                    free(opt);
                    usage_and_exit(argv[0]);
                } else if (argno + 1 == argc - 1) {
                    fprintf(stderr, "Missing input file.\n");
                    free(opt);
                    usage_and_exit(argv[0]);
                }

                ret = true;
                opt->output_file = argv[argno + 1];
                if (STREQUALS(opt->output_file, "-")) {
                    opt->output_file = "/dev/stdin";
                }
                break;
            default:
                usage_and_exit(argv[0]);
        }
    }

    return ret;
}

static void help_and_exit(const char *program_name)
{
    printf("Usage:\n"
           "   %s [-a | -r] [-i [-n]] [-f] [-d hard drive file] [-j journal-file] [-o output-file] input-file\n"
           "   %s [--help | --version]\n\n"
           " If neither of `-a' or `-r' is set, then the program will perform both.\n"
           "  -a, --assemble     Only assemble the input file.\n"
           "  -r, --run          Run the specified input file.\n"
           "  -i, --interactive  Run the emulator in interactive mode.\n"
           "  -n, --no-colors    When running interactively, disable color in prompts.\n"
           "  -f, --force        Ignore page permissions during execution.\n"
           "  -d, --hdd          Use the following file as the hard drive.\n"
           "  -j, --journal      Verbosely log output to this file.\n"
           "  -o, --output       Set the output file to something other than `a.out'.\n",
           program_name,
           program_name);
    cleanup(0);
}

static void usage_and_exit(const char *program_name)
{
    printf("Usage:\n"
           "   %s [-a | -r] [-i [-n]] [-f] [-d hard drive file] [-j journal-file] [-o output-file] input-file\n"
           "   %s [--help | --version]\n",
           program_name,
           program_name);
    cleanup(1);
}

static void version_and_exit()
{
    printf("antz assembler and emulator version %s.\n\n"
           "antz is free software: you can redistribute it and/or modify\n"
           "it under the terms of the GNU General Public License as published by\n"
           "the Free Software Foundation, either version 2 of the License, or\n"
           "(at your option) any later version.\n",
           VERSION_STRING);
    cleanup(0);
}

