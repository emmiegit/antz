/*
 * signal_handler.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include <stdio.h>

#include "signal_handler.h"
#include "main.h"

static void handle_signal(int signum);

void set_up_signal_handlers()
{
    signal(SIGTERM, handle_signal);
    signal(SIGHUP, handle_signal);
    signal(SIGINT, handle_signal);
    signal(SIGSEGV, handle_signal);
    signal(SIGPIPE, handle_signal);
}

static void handle_signal(int signum)
{
    switch (signum) {
        case SIGTERM:
            fprintf(stderr, "Received termination signal, exiting...\n");
            cleanup(0);
            break;
        case SIGHUP:
            fprintf(stderr, "Received hangup signal, exiting...\n");
            cleanup(0);
            break;
        case SIGINT:
            fprintf(stderr, "Received interrupt signal, exiting...\n");
            cleanup(1);
            break;
        case SIGSEGV:
            fprintf(stderr, "Segmentation fault. Dumping core (if enabled)...\n");
            cleanup(1);
            break;
        case SIGPIPE:
            fprintf(stderr, "Broken pipe. Ignoring.\n");
    }
}

