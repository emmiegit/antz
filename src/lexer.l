%{
#include <stddef.h>
#include <stdio.h>

#include "assembly_util.h"
#include "parser.h"
#include "y.tab.h"

#define yyinit()
#define yyterminate() return END;

%}

%option outfile="src/lexer.c" header-file="src/lexer.h"
%option warn nodefault
%option reentrant noyywrap bison-bridge

STRING      "\"([^\\\n]|\\.{1,3}?)*\""
CHAR        "'([^\\\n]|\\.{1,3}?)'"
HEX_NUM     "[+-]?0[xX][A-Fa-f0-9]+"
OCTAL_NUM   "[+-]?0[oO][0-7]+"
BINARY_NUM  "[+-]?0[bB][01]+"
DECIMAL_NUM "[+-]?[0-9]+"
REGISTER    "[rR]([0-9]+|[A-Za-z]{2})"
IDENTIFIER  "[A-Za-z_][A-Za-z_0-9]*"
COLON       ":"
OPCODE      "\.?[A-Za-z]+"
COMMA       ","
COMMENT     ";.*$"

%%
{STRING}        {
                    yylval->strval = parse_string_literal(yytext, NULL, NULL);
                    return STRING;
                }

{CHAR}          {
                    yylval->intval = parse_character_literal(yytext, NULL);
                    return INT;
                }

{HEX_NUM}       {
                    yylval->intval = parse_hex_number(yytext, NULL);
                    return INT;
                }

{OCTAL_NUM}     {
                    yylval->intval = parse_octal_number(yytext, NULL);
                    return INT;
                }

{BINARY_NUM}    {
                    yylval->intval = parse_binary_number(yytext, NULL);
                    return INT;
                }

{DECIMAL_NUM}   {
                    yylval->intval = parse_decimal_number(yytext, NULL);
                    return INT;
                }

{REGISTER}      {
                    yylval->reg = parse_register(yytext, NULL);
                    return REGISTER;
                }

{IDENTIFIER}    {
                    return IDENTIFIER;
                }

{COLON}         {
                    return COLON;
                }

{OPCODE}        {
                    return OPCODE;
                }

{COMMA}         {
                    return COMMA;
                }

\n              {
                    return NEWLINE;
                }

{COMMENT}       /* ignore comments */;

[ \r\t]+          /* ignore whitespace */;

.               {
                    yyerror(NULL, &status, "unexpected token");
                }
%%


 
{WS}            { /* Skip blanks. */ }
{NUMBER}        { sscanf(yytext, "%d", &yylval->value); return TOKEN_NUMBER; }
 
{MULTIPLY}      { return TOKEN_MULTIPLY; }
{DIVIDE}        { return TOKEN_DIVIDE; }
{PLUS}          { return TOKEN_PLUS; }
{SUBTRACT}      { return TOKEN_SUBTRACT; }
{LPAREN}        { return TOKEN_LPAREN; }
{RPAREN}        { return TOKEN_RPAREN; }
.               { yyerror(NULL, NULL, yytext); }
 

