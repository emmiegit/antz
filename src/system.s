; System calls and BIOS instructions

.data

; Peripherals table
.orig 0x0000
    .block 0x00f

; Trap vector table
.orig 0x0010 ; getc
    .addrof __trap_getc

.orig 0x0011 ; putc
    .addrof __trap_putc

.orig 0x0012 ; mvcur
    .addrof __trap_mvcur

.orig 0x0013 ; clscr
    .addrof __trap_clscr

.orig 0x0021 ; puts
    .addrof __trap_puts

.orig 0x0fff ; halt
    .addrof __trap_halt

; Executable page table
.orig 0x1000
    .block 0xfff

; Service routine implementations
.orig 0xfffffe00
.text
    __trap_getc:
        ldi rio, __dev_keyboard
        jez rio, __trap_getc        ; poll keyboard
        sti rio, __dev_keyboard     ; signal that we got the character
        ret

    __trap_putc:
        st r0, __trap_backup_putc   ; backup r0
    __trap_putc_poll:
        ldi r0, __dev_monitor
        jez r0, __trap_putc         ; poll monitor
        sti rio, __dev_monitor      ; write character
    __trap_putc_finish:
        ld r0, __trap_backup_putc
        ret                         ; load backup and return

    __trap_mvcur:
        st rio, __trap_backup0
        st r0, __trap_backup1
        st r1, __trap_backup2
        st rrt, __trap_backup3      ; do register backups
        rshf r0, rio, 4             ; store x in r0
        and r1, rio, 0xffff         ; store y in r1
        set rio, '\033'
        putc                        ; print '\033'
        set rio, '['
        putc                        ; print '['
        mov rio, r0
        call __sub_print_decimal        ; print r0
        set rio, ';'
        putc                        ; print ';'
        mov rio, r1
        call __sub_print_decimal
        set rio, 'H'
        putc                        ; print 'H'
    __trap_mvcur_finish:
        ld rio, __trap_backup0
        ld r0, __trap_backup1
        ld r1, __trap_backup2
        ld rrt, __trap_backup3
        ret                         ; load backups and return

    __trap_clscr:
        st rio, __trap_backup0
        st rrt, __trap_backup1      ; do register backups
        set rio, '\033'
        putc                        ; print '\033'
        set rio, 'c'
        putc                        ; print 'c'
    __trap_clscr_finish:
        ld rio, __trap_backup0
        ld rrt, __trap_backup1
        ret                         ; load backups and return

    __trap_puts:
        st rio, __trap_backup0
        st r0, __trap_backup1
        st rrt, __trap_backup2      ; do register backups
        mov r0, rio                 ; store the pointer in r0
    __trap_puts_loop:
        ldr rio, r0, 0              ; rio <- mem[r0]
        jez rio, __trap_puts_finish ; if (rio == '\0') finish
        putc
        add r0, r0, 1               ; increment the pointer
        jmp __trap_puts_loop
    __trap_puts_finish:
        ld rio, __trap_backup0
        ld r0, __trap_backup1
        ld rrt, __trap_backup2
        ret                         ; load backups and return

    __trap_halt:
        ; This instruction must be caught by hardware.
        ; If we get this far, it means an error occurred.
        ; Loop infinitely to not cause more damage.
        jmp __trap_halt

.data
    __dev_keyboard:     .fill 0xfffffff0
    __dev_monitor:      .fill 0xfffffff1
    __dev_hdd_seek:     .fill 0xfffffff2
    __dev_hdd_data:     .fill 0xfffffff3
    __trap_backup_putc: .zfill
    __trap_backup0:     .zfill
    __trap_backup1:     .zfill
    __trap_backup2:     .zfill
    __trap_backup3:     .zfill

.text
    __sub_print_decimal:
        st rio, __sub_print_decimal_backup_rio
        st rrt, __sub_print_decimal_backup_rrt
        st r0, __sub_print_decimal_backup_r0
        st r1, __sub_print_decimal_backup_r1
        mov r0, rio
        set r1, 1
        jge r0, __sub_print_decimal_ten_loop
    __sub_print_decimal_print_neg:
        set rio, '-'
        not r0, r0
        add r0, r0, 1
        putc
    __sub_print_decimal_ten_loop:
        mul r1, r1, 10
        cmp rio, r1, r0
        jlz __sub_print_decimal_ten_loop
    __sub_print_decimal_print_loop:
        div rio, r0, r1
        sub rio, rio, '0'
        putc
        mod r0, r0, r1
        div r1, r1, 10
        jgz r1, __sub_print_decimal_print_loop
    __sub_print_decimal_finish:
        ld rio, __sub_print_decimal_backup_rio
        ld r0, __sub_print_decimal_backup_r0
        ld r1, __sub_print_decimal_backup_r1
        ret

.data
    __sub_print_decimal_backup_rio:     .zfill
    __sub_print_decimal_backup_rrt:     .zfill
    __sub_print_decimal_backup_r0:      .zfill
    __sub_print_decimal_backup_r1:      .zfill

; Device register mappings
.orig 0xfffffff0
    .block 0xf

.end

; vim: set ft=antz:
