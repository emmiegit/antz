%{
#include <stdint.h>
#include <stdio.h>

#include "assembly.h"
#include "assembly_util.h"
#include "char_vector.h"
#include "emulator.h"

yyparse(yyscan_t *scanner);
yylex(YYSTYPE *yylval_param, yyscan_t scanner);

void yyerror(yyscan_t scanner, struct parse_status *status, const char *message)
{
    print_parse_message(status, "error: %s.\n", message);
}
%}

%code requires {

#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
typedef void* yyscan_t;
#endif

}

%defines "src/parser.h"
%define api.pure
%lex-param {yyscan_t scanner}
%parse-param {yyscan_t scanner}
%parse-param {struct parse_status *status}

%union {
    reg reg;
    int32_t intval;
    const char *strval;
}

%token <reg> REGISTER
%token <intval> INT
%token <strval> STRING
%token <strval> IDENTIFIER
%token COLON
%token OPCODE
%token COMMA
%token NEWLINE
%token END

%%
program
    : /* empty */
    | program line
    ;

line
    : IDENTIFIER COLON instruction NEWLINE      { $$ = create_line($1, $3); }
    | IDENTIFIER COLON NEWLINE                  { $$ = create_line($1, NULL); }
    | instruction NEWLINE                       { $$ = create_line(NULL, $1); }
    ;

instruction
    : OPCODE                                    { $$ = create_instruction($1, NULL); }
    | OPCODE operands                           { $$ = create_instruction($1, $2); }
    ;

operands
    : literal                                   { $$ = join_operands }
    | operands COMMA literal                    {}
    ;

literal
    : STRING                                    {}
    | INT                                       {}
    | REGISTER                                  {}
    ;

%%

%left '+' TOKEN_PLUS
%left '-' TOKEN_MINUS
%left '*' TOKEN_MULTIPLY
%left '/' TOKEN_DIVIDE
 
%token TOKEN_LPAREN
%token TOKEN_RPAREN
%token TOKEN_PLUS
%token TOKEN_SUBTRACT
%token TOKEN_MULTIPLY
%token TOKEN_DIVIDE
%token <value> TOKEN_NUMBER

%type <expression> expr
 
 
input
    : expr { *expression = $1; }
    ;
 
expr
    : expr[L] TOKEN_PLUS expr[R] { $$ = create_operation(ADD_TYPE, $L, $R); }
    | expr[L] TOKEN_SUBTRACT expr[R] { $$ = create_operation(SUBTRACT_TYPE, $L, $R); }
    | expr[L] TOKEN_MULTIPLY expr[R] { $$ = create_operation(MULTIPLY_TYPE, $L, $R); }
    | expr[L] TOKEN_DIVIDE expr[R] { $$ = create_operation(DIVIDE_TYPE, $L, $R); }
    | TOKEN_LPAREN expr[E] TOKEN_RPAREN { $$ = $E; }
    | TOKEN_NUMBER { $$ = create_number($1); }
    ;
 
