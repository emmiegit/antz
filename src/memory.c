/*
 * memory.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "device.h"
#include "int_hashmap.h"
#include "journal.h"
#include "main.h"
#include "memory.h"
#include "opcode.h"

/* Static declarations */
static struct mem_page *get_page(struct memory *mem, size_t pageno, bool create_if_nonexistant);
static bool set_page_executable(struct memory *mem, size_t pageno);
static bool unset_page_executable(struct memory *mem, size_t pageno);
static bool check_page_executable(struct memory *mem, size_t pageno);

/* Function implementations */
struct memory *memory_init()
{
    struct memory *mem = malloc(sizeof(struct memory));
    journal("Initializing virtual memory struct.\n");

    mem->pages = int_hashmap_init_c(14);
    if (mem->pages == NULL) {
        free(mem);
        return NULL;
    }

    mem->mappings = int_hashmap_init_c(8);
    if (mem->mappings == NULL) {
        int_hashmap_destroy(mem->pages);
        free(mem);
        return NULL;
    }

    mem->last_page = NULL;
    return mem;
}

void memory_destroy(struct memory *mem)
{
    journal("Destroying virtual memory struct.\n");

    /* Free all page data */
    const long *keys = int_hashmap_list_keys(mem->pages);
    struct mem_page *page;
    size_t i;
    bool ret;

    for (i = 0; i < mem->pages->size; i++) {
        journal("Freeing page %zd.\n", i);
        ret = int_hashmap_get(mem->pages, keys[i], (const void **)(&page));
        assert(ret);
        free(page->data);
        free(page);
    }
    free((long *)keys);

    /* Free the hashmaps */
    int_hashmap_destroy(mem->pages);
    int_hashmap_destroy(mem->mappings);
    free(mem);
}

bool memory_set_executable(struct memory *mem, uint32_t start_addr, uint32_t end_addr, bool executable)
{
    if (end_addr >= start_addr) {
        return false;
    }

    uint32_t addr;
    size_t pageno;
    for (addr = start_addr; addr < end_addr; addr += PAGE_SIZE) {
        pageno = addr / PAGE_SIZE;

        if (executable) {
            journal("Setting page %zd to be executable.\n", pageno);
            return set_page_executable(mem, pageno);
        } else {
            journal("Setting page %zd to be not executable.\n", pageno);
            return unset_page_executable(mem, pageno);
        }
    }

    return false;
}

bool memory_map(struct memory *mem, uint32_t address, struct device_handler *dev)
{
    return int_hashmap_put(mem->mappings, address, dev);
}

bool memory_unmap(struct memory *mem, uint32_t address)
{
    return int_hashmap_remove(mem->mappings, address);
}

uint32_t read_word(struct memory *mem, uint32_t address)
{
    journal("Reading word at 0x%08x.\n", address);
    struct mem_page *page = get_page(mem, address / PAGE_SIZE, false);

    return (page == NULL) ? 0x00 : (page->data[address % PAGE_SIZE]);
}

uint32_t read_executable_word(struct program_state *state, uint32_t address)
{
    journal("Reading instruction at 0x%08x.\n", address);
    struct mem_page *page = get_page(state->mem, address / PAGE_SIZE, false);
    uint32_t word = (page == NULL) ? 0x00 : (page->data[address % PAGE_SIZE]);

    if (!page || !check_page_executable(state->mem, page->pageno)) {
        set_register(state, REGISTER_RER, RERROR_CANNOT_EXECUTE);
        journal("Cannot execute, permission denied. Attempted to run 0x%08x at 0x%08x.\n",
                word,
                address);
        if (state->opt->check_page_mode) {
            return INSTRUCTION_HALT;
        } else {
            journal("Ignoring mode.\n");
        }
    }

    return word;
}

uint8_t read_byte(struct memory *mem, uint32_t address, unsigned int byte)
{
    journal("Reading byte %zd at 0x%08x.\n", byte, address);
    int32_t word = read_word(mem, address);
    return (word >> (byte * 8)) & 0xff;
}

void write_word(struct memory *mem, uint32_t address, uint32_t value)
{
    journal("Writing word \"0x%08x\" to 0x%08x.\n", value, address);
    struct device_handler *dev;
    struct mem_page *page;

    if (int_hashmap_get(mem->mappings, address, (const void **)(&dev))) {
        journal("Address is memory mapped, writing to device %d.\n", dev->id);
        dev->dev_read_word(value);
    }

    if (value) {
        page = get_page(mem, address / PAGE_SIZE, true);
        page->data[address % PAGE_SIZE] = value;
    } else {
        /* If the page doesn't exist, the word is already a zero */
        page = get_page(mem, address / PAGE_SIZE, false);

        if (page) {
            page = get_page(mem, address / PAGE_SIZE, true);
            page->data[address % PAGE_SIZE] = value;
        }
    }
}

void write_byte(struct memory *mem, uint32_t address, unsigned int byte, uint8_t value)
{
    journal("Writing byte \"0x%02x\" to %zd.\n", value, address);
    struct device_handler *dev;
    struct mem_page *page;
    int32_t mask = 0xff << (byte * 8);

    if (int_hashmap_get(mem->mappings, address, (const void **)(&dev))) {
        journal("Address is memory mapped, writing to device %d.\n", dev->id);
        int32_t shifted_value = ((int32_t)value & 0xff) << (byte * 8);
        int32_t word = dev->dev_read_word();
        word = (word & ~mask) | shifted_value;
        dev->dev_write_word(word);
    }

    if (value) {
        page = get_page(mem, address / PAGE_SIZE, true);
        int32_t shifted_value = ((int32_t)value & 0xff) << (byte * 8);
        int32_t *word = &page->data[address % PAGE_SIZE];
        *word = (*word & ~mask) | shifted_value;
    } else {
        /*
         * If the page doesn't exist, the byte ready a zero.
         * Otherwise, just zero it.
         */
        page = get_page(mem, address / PAGE_SIZE, false);

        if (page) {
            page->data[address % PAGE_SIZE] &= ~mask;
        }
    }
}

static struct mem_page *get_page(struct memory *mem, size_t pageno, bool create_if_nonexistant)
{
    /* Check cached page */
    if (mem->last_page && mem->last_page->pageno == pageno) {
        journal("Page %zd cached, returning.\n", pageno);
        return mem->last_page;
    }

    /* Look for existing page */
    struct mem_page *page;
    bool ret = int_hashmap_get(mem->pages, pageno, (const void **)(&page));
    if (ret) {
        journal("Page %zd found, returning from map.\n", pageno);
        mem->last_page = page;
        return page;
    }

    /* Page doesn't exist, but don't create it */
    if (!create_if_nonexistant) {
        journal("Page %zd not found, returning blank page.\n", pageno);
        return NULL;
    }

    /* Page doesn't exist, create it */
    journal("Page %zd not found, creating new page.\n", pageno);
    page = malloc(sizeof(struct mem_page));
    if (page == NULL) {
        fprintf(stderr, "Unable to allocate new page: %s\n",
                strerror(errno));
        cleanup(1);
    }

    page->pageno = pageno;
    page->data = calloc(PAGE_SIZE, sizeof(int32_t));
    if (page->data == NULL) {
        fprintf(stderr, "Unable to allocate new page data: %s\n",
                strerror(errno));
        cleanup(1);
    }

    /* Add page to virtual memory system */
    ret = int_hashmap_put(mem->pages, pageno, page);
    assert(ret);

    mem->last_page = page;
    return page;
}

static bool set_page_executable(struct memory *mem, size_t pageno)
{
    if (pageno == 0) {
        return false;
    }

    size_t address = EXECUTABLE_PAGE_TABLE_START + (pageno % EXECUTABLE_PAGE_TABLE_SIZE);
    uint32_t word;
    for (; address < EXECUTABLE_PAGE_TABLE_END; address++) {
        word = read_word(mem, address);

        if (word == 0) {
            write_word(mem, address, pageno);
            return true;
        }
    }

    return false;
}

static bool unset_page_executable(struct memory *mem, size_t pageno)
{
    if (pageno == 0) {
        return false;
    }

    size_t address = EXECUTABLE_PAGE_TABLE_START + (pageno % EXECUTABLE_PAGE_TABLE_SIZE);
    uint32_t word;
    for (; address < EXECUTABLE_PAGE_TABLE_END; address++) {
        word = read_word(mem, address);

        if (word == pageno) {
            write_word(mem, address, 0);
            return true;
        }
    }

    return false;
}

static bool check_page_executable(struct memory *mem, size_t pageno)
{
    if (pageno == 0) {
        return false;
    }

    size_t address = EXECUTABLE_PAGE_TABLE_START + (pageno % EXECUTABLE_PAGE_TABLE_SIZE);
    uint32_t word;
    for (; address < EXECUTABLE_PAGE_TABLE_END; address++) {
        word = read_word(mem, address);

        if (word == pageno) {
            return true;
        } else if (word == 0) {
            return false;
        }
    }

    return false;
}

