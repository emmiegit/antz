/*
 * device.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DEVICE_H
#define __DEVICE_H

#include <stdbool.h>
#include <stdint.h>

#include "emulator.h"

struct device_handler {
    uint8_t id;
    int32_t (*dev_read_word)();
    void (*dev_write_word)(int32_t);
};

void init_raw_keyboard();
void deinit_raw_keyboard();
bool kbhit(char *ch);

void register_keyboard(struct program_state *state);
void register_monitor(struct program_state *state);
void register_hdd(struct program_state *state);

#endif /* __DEVICE_H */

/* vim: set ft=c: */
