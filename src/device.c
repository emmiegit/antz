/*
 * device.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

#include "device.h"
#include "journal.h"
#include "memory.h"

#define UNUSED(x)       ((void)(x))

/* Static variables */
static bool keyboard_init = false;
static struct termios oldterm, newterm;
static int oldfd;
static char keyboard_reg;

/* Static declarations */
static int32_t keyboard_read_word();
static int32_t monitor_read_word();
static int32_t hdd_seek_read_word();
static int32_t hdd_data_read_word();
static void keyboard_write_word(int32_t word);
static void monitor_write_word(int32_t word);
static void hdd_seek_write_word(int32_t word);
static void hdd_data_write_word(int32_t word);

#define KEYBOARD_MAP_ADDRESS    0xfffffff0
static struct device_handler keyboard_dev = {
    .id = 0,
    .dev_read_word = keyboard_read_word,
    .dev_write_word = keyboard_write_word,
};

#define MONITOR_MAP_ADDRESS     0xfffffff1
static struct device_handler monitor_dev = {
    .id = 1,
    .dev_read_word = monitor_read_word,
    .dev_write_word = monitor_write_word,
};

#define HDD_SEEK_MAP_ADDRESS    0xfffffff2
static struct device_handler hdd_seek_dev = {
    .id = 2,
    .dev_read_word = hdd_seek_read_word,
    .dev_write_word = hdd_seek_write_word,
};

#define HDD_DATA_MAP_ADDRESS    0xfffffff3
static struct device_handler hdd_data_dev = {
    .id = 3,
    .dev_read_word = hdd_data_read_word,
    .dev_write_word = hdd_data_write_word,
};

void register_keyboard(struct program_state *state)
{
    journal("Mapping keyboard to 0x%08x.\n", KEYBOARD_MAP_ADDRESS);
    memory_map(state->mem, (uint32_t)KEYBOARD_MAP_ADDRESS, &keyboard_dev);
}

static int32_t keyboard_read_word()
{
    journal("Reading from keyboard register.\n");
    char ch;
    if (!keyboard_reg) {
        if (kbhit(&ch)) {
            keyboard_reg = ch;
        }
    }

    return keyboard_reg;
}

static void keyboard_write_word(int32_t word)
{
    journal("Writing to keyboard register.\n");
    UNUSED(word);
    keyboard_reg = 0;
}

void register_monitor(struct program_state *state)
{
    journal("Mapping monitor to 0x%08x.\n", MONITOR_MAP_ADDRESS);
    memory_map(state->mem, (uint32_t)MONITOR_MAP_ADDRESS, &monitor_dev);
}

static int32_t monitor_read_word()
{
    journal("Reading from monitor register.\n");
    return 0;
}

static void monitor_write_word(int32_t word)
{
    journal("Writing to monitor register.\n");
    putchar((char)word);
}

void register_hdd(struct program_state *state)
{
    journal("Mapping hard drive to 0x%08x (seek) and 0x%08x (data).\n",
            HDD_SEEK_MAP_ADDRESS, HDD_DATA_MAP_ADDRESS);
    memory_map(state->mem, (uint32_t)HDD_SEEK_MAP_ADDRESS, &hdd_seek_dev);
    memory_map(state->mem, (uint32_t)HDD_DATA_MAP_ADDRESS, &hdd_data_dev);
}

static int32_t hdd_seek_read_word()
{
    journal("Reading from hard drive seek register.\n(Not implemented yet)\n");
    /* TODO file io */
    return -1;
}

static void hdd_seek_write_word(int32_t word)
{
    journal("Writing to hard drive seek register.\n(Not implemented yet)\n");
    /* TODO */
    UNUSED(word);
}

static int32_t hdd_data_read_word()
{
    journal("Reading from hard drive data register.\n(Not implemented yet)\n");
    /* TODO */
    return -1;
}

static void hdd_data_write_word(int32_t word)
{
    journal("Writing to hard drive data register.\n(Not implemented yet)\n");
    /* TODO */
    UNUSED(word);
}

void init_raw_keyboard()
{
    if (!keyboard_init) {
        journal("Initializing raw keyboard.\n");
        tcgetattr(STDIN_FILENO, &oldterm);
        newterm = oldterm;
        newterm.c_lflag &= ~(ICANON | ECHO);
        tcsetattr(STDIN_FILENO, TCSANOW, &newterm);
        oldfd = fcntl(STDIN_FILENO, F_GETFL, 0);
        fcntl(STDIN_FILENO, F_SETFL, oldfd | O_NONBLOCK);
    } else {
        journal("Keyboard already initialized.\n");
    }
}

void deinit_raw_keyboard()
{
    if (keyboard_init) {
        journal("Deinitializing raw keyboard.\n");
        tcsetattr(STDIN_FILENO, TCSANOW, &oldterm);
        fcntl(STDIN_FILENO, F_SETFL, oldfd);
    } else {
        journal("Keyboard not initialized.\n");
    }
}

bool kbhit(char *ch)
{
    if (!keyboard_init) {
        init_raw_keyboard();
        *ch = getchar();
        deinit_raw_keyboard();
    } else {
        *ch = getchar();
    }

    return (*ch != EOF);
}

