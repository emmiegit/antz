/*
 * emulator.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EMULATOR_H
#define __EMULATOR_H

#include <stdbool.h>
#include <stdint.h>

#include "arguments.h"

typedef unsigned char reg;

#define REGISTER_R0                 0
#define REGISTER_R1                 1
#define REGISTER_R2                 2
#define REGISTER_R3                 3
#define REGISTER_R4                 4
#define REGISTER_R5                 5
#define REGISTER_R6                 6
#define REGISTER_R7                 7
#define REGISTER_R8                 8
#define REGISTER_R9                 9
#define REGISTER_R10                10
#define REGISTER_R11                11
#define REGISTER_RIO                12  /* I/O register */
#define REGISTER_RRT                13  /* Return address register */
#define REGISTER_RER                14  /* Error number register */
#define REGISTER_RPC                15  /* Program counter */

extern const char *const REGISTER_NAMES[];
#define get_register_name(x)        (((x) <= REGISTER_RPC) ? REGISTER_NAMES[x] : "r??")

#define NAMED_ERROR_COUNT           8
#define RERROR_OK                   0
#define RERROR_ILLEGAL_OPERATION    1
#define RERROR_ARITHMETIC           2
#define RERROR_IO_ERROR             3
#define RERROR_TTY_ERROR            4
#define RERROR_PERMISSION_DENIED    5
#define RERROR_CANNOT_EXECUTE       6
#define RERROR_OUT_OF_MEMORY        7
#define RERROR_INTERNAL             999

extern const char *const ERROR_NAMES[];
#define get_error_name(x)          (((x) < NAMED_ERROR_COUNT) ? ERROR_NAMES[x] : "Unknown error")

struct program_state {
    /* Memory */
    struct memory *mem;

    /* Registers */
    int32_t registers[16];

    /* Emulator state */
    const options *opt;
    reg last_register;

    /* Execution mode */
    bool supervisor;
};

void run_interactively(struct program_state *state);
bool run_next_instruction(struct program_state *state);
void set_register(struct program_state *state, reg register_id, int32_t value);
int32_t get_register(struct program_state *state, reg register_id);

#endif /* __EMULATOR_H */

/* vim: set ft=c: */
