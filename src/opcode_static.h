/*
 * static_opcode.h
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OPCODE_STATIC_H
#define __OPCODE_STATIC_H

#include <stdint.h>

#include "emulator.h"

static void run_add(struct program_state *state, const uint32_t instruction);
static void run_and(struct program_state *state, const uint32_t instruction);
static void run_call(struct program_state *state, const uint32_t instruction);
static void run_cmp(struct program_state *state, const uint32_t instruction);
static void run_div(struct program_state *state, const uint32_t instruction);
static void run_fadd(struct program_state *state, const uint32_t instruction);
static void run_fcmp(struct program_state *state, const uint32_t instruction);
static void run_fdiv(struct program_state *state, const uint32_t instruction);
static void run_fmul(struct program_state *state, const uint32_t instruction);
static void run_fneg(struct program_state *state, const uint32_t instruction);
static void run_jez(struct program_state *state, const uint32_t instruction);
static void run_jge(struct program_state *state, const uint32_t instruction);
static void run_jgz(struct program_state *state, const uint32_t instruction);
static void run_jle(struct program_state *state, const uint32_t instruction);
static void run_jlz(struct program_state *state, const uint32_t instruction);
static void run_jmp(struct program_state *state, const uint32_t instruction);
static void run_jnz(struct program_state *state, const uint32_t instruction);
static void run_jro(struct program_state *state, const uint32_t instruction);
static void run_ld(struct program_state *state, const uint32_t instruction);
static void run_ldb(struct program_state *state, const uint32_t instruction);
static void run_ldi(struct program_state *state, const uint32_t instruction);
static void run_ldr(struct program_state *state, const uint32_t instruction);
static void run_lea(struct program_state *state, const uint32_t instruction);
static void run_lshf(struct program_state *state, const uint32_t instruction);
static void run_mod(struct program_state *state, const uint32_t instruction);
static void run_movb(struct program_state *state, const uint32_t instruction);
static void run_mul(struct program_state *state, const uint32_t instruction);
static void run_not(struct program_state *state, const uint32_t instruction);
static void run_or(struct program_state *state, const uint32_t instruction);
static void run_rshf(struct program_state *state, const uint32_t instruction);
static void run_set(struct program_state *state, const uint32_t instruction);
static void run_st(struct program_state *state, const uint32_t instruction);
static void run_stb(struct program_state *state, const uint32_t instruction);
static void run_sti(struct program_state *state, const uint32_t instruction);
static void run_str(struct program_state *state, const uint32_t instruction);
static void run_sub(struct program_state *state, const uint32_t instruction);
static void run_trap(struct program_state *state, const uint32_t instruction);
static void run_xor(struct program_state *state, const uint32_t instruction);

static void print_add(const uint32_t instruction);
static void print_and(const uint32_t instruction);
static void print_call(const uint32_t instruction);
static void print_cmp(const uint32_t instruction);
static void print_div(const uint32_t instruction);
static void print_fadd(const uint32_t instruction);
static void print_fcmp(const uint32_t instruction);
static void print_fdiv(const uint32_t instruction);
static void print_fmul(const uint32_t instruction);
static void print_fneg(const uint32_t instruction);
static void print_jez(const uint32_t instruction);
static void print_jge(const uint32_t instruction);
static void print_jgz(const uint32_t instruction);
static void print_jle(const uint32_t instruction);
static void print_jlz(const uint32_t instruction);
static void print_jmp(const uint32_t instruction);
static void print_jnz(const uint32_t instruction);
static void print_jro(const uint32_t instruction);
static void print_ld(const uint32_t instruction);
static void print_ldb(const uint32_t instruction);
static void print_ldi(const uint32_t instruction);
static void print_ldr(const uint32_t instruction);
static void print_lea(const uint32_t instruction);
static void print_lshf(const uint32_t instruction);
static void print_mod(const uint32_t instruction);
static void print_movb(const uint32_t instruction);
static void print_mul(const uint32_t instruction);
static void print_not(const uint32_t instruction);
static void print_or(const uint32_t instruction);
static void print_rshf(const uint32_t instruction);
static void print_set(const uint32_t instruction);
static void print_st(const uint32_t instruction);
static void print_stb(const uint32_t instruction);
static void print_sti(const uint32_t instruction);
static void print_str(const uint32_t instruction);
static void print_sub(const uint32_t instruction);
static void print_trap(const uint32_t instruction);
static void print_xor(const uint32_t instruction);

#endif /* __OPCODE_STATIC_H */

