/*
 * assembly_util.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assembly_util.h"
#include "emulator.h"

#define STREMPTY(x)                 ((x)[0] == '\0')
#define STREQUALS(x, y)             (strcmp((x), (y)) == 0)
#define STREQUALS2(x, y, len)       (strncmp((x), (y), (len)) == 0)

#define MIN_SIGNED_INT32_VALUE      -2147483648  /* -(1 << 31) */
#define MAX_SIGNED_INT32_VALUE      2147483647   /* (1 << 31) - 1 */
#define MAX_UNSIGNED_INT32_VALUE    4294967295   /* (1 << 32) - 1 */
#define DEFAULT_STRING_CAPACITY     30

static char parse_character(const char *str, size_t *length, struct parse_status *status);
static char parse_octal(const char *str, size_t length, struct parse_status *status);
static char parse_hex(const char *str, size_t length, struct parse_status *status);

/*
 * Full regex:
 * [rR]([0-9]+|[iI][oO]|[rR][tT]|[eE][rR]|[pP][cC])
 */
reg parse_register(const char *str, struct parse_status *status)
{
    assert(str[0] == 'r' || str[0] == 'R');

    /* Specially named registers */
    if (string_equals_ignore_case(str + 1, "io")) {
        status->success = true;
        return REGISTER_RIO;
    } else if (string_equals_ignore_case(str + 1, "rt")) {
        status->success = true;
        return REGISTER_RRT;
    } else if (string_equals_ignore_case(str + 1, "er")) {
        status->success = true;
        return REGISTER_RER;
    } else if (string_equals_ignore_case(str + 1, "pc")) {
        status->success = true;
        return REGISTER_RPC;
    }

    /* General-purpose registers */
    unsigned int reg = 0;
    size_t i;
    for (i = 1; str[i]; i++) {
        if (!isdigit(str[i])) {
            print_parse_message(status, "error: Invalid register: \"%s\".\n", str);
            status->success = false;
            return 0;
        }

        reg *= 10;
        reg += str[i] - '0';
    }

    if (reg > REGISTER_R11) {
        print_parse_message(status, "error: Invalid register: \"%s\".\n", str);
        status->success = false;
        return 0;
    }

    status->success = true;
    return reg;
}

/*
 * Full regex:
 * [+-]?0[xX][A-Fa-f0-9]+
 */
int32_t parse_hex_number(const char *str, struct parse_status *status)
{
    unsigned long value = 0;
    bool is_negative;
    size_t i;
    if (str[0] == '-') {
        is_negative = true;
        i = 3;
        assert(STREQUALS2(str + 1, "0x", 2) || STREQUALS2(str + 1, "0X", 2));
    } else if (str[0] == '+') {
        is_negative = false;
        i = 3;
        assert(STREQUALS2(str + 1, "0x", 2) || STREQUALS2(str + 1, "0X", 2));
    } else {
        is_negative = false;
        i = 2;
        assert(STREQUALS2(str, "0x", 2) || STREQUALS2(str, "0X", 2));
    }

    for (; str[i]; i++) {
        if (!isxdigit(str[i])) {
            print_parse_message(status, "error: '%c' is not a hex digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value *= 0x10;
        value += (isdigit(str[i])) ? (str[i] - '0') : (tolower(str[i]) - 'a' + 10);
    }

    if (is_negative) {
        value = -value;
    }

    if (value > MAX_UNSIGNED_INT32_VALUE) {
        print_parse_message(status, "error: Hex number is too wide to fit into a 32-bit word: %s\n",
                str);
        status->success = false;
        return 0;
    }

    status->success = true;
    return value;
}

/*
 * Full regex:
 * [+-]?0[bB][01]+
 */
int32_t parse_binary_number(const char *str, struct parse_status *status)
{
    assert(!STREMPTY(str));

    unsigned long value = 0;
    bool is_negative;
    size_t i;

    if (str[0] == '-') {
        is_negative = true;
        assert(STREQUALS2(str + 1, "0b", 2) || STREQUALS2(str + 1, "0B", 2));
        i = 3;
    } else if (str[0] == '+') {
        is_negative = false;
        assert(STREQUALS2(str + 1, "0b", 2) || STREQUALS2(str + 1, "0B", 2));
        i = 3;
    } else {
        is_negative = false;
        assert(STREQUALS2(str, "0b", 2) || STREQUALS2(str, "0B", 2));
        i = 2;
    }

    for (; str[i]; i++) {
        if (str[i] != '0' && str[i] != '1') {
            print_parse_message(status, "error: '%c' is not a binary digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value <<= 1;
        if (str[i] == '1') {
            value++;
        }
    }

    if (is_negative) {
        value = -value;
    }

    if (value > MAX_UNSIGNED_INT32_VALUE) {
        print_parse_message(status, "error: Binary number is too wide to fit into a 32-bit word: %s\n",
                str);
        status->success = false;
        return 0;
    }

    status->success = true;
    return value;
}

/*
 * Full regex:
 * [+-]?0[oO][01]+
 */
int32_t parse_octal_number(const char *str, struct parse_status *status)
{
    assert(!STREMPTY(str));

    unsigned long value = 0;
    bool is_negative;
    size_t i;

    if (str[0] == '-') {
        is_negative = true;
        assert(STREQUALS2(str + 1, "0o", 2) || STREQUALS2(str + 1, "0O", 2));
        i = 3;
    } else if (str[0] == '+') {
        is_negative = false;
        assert(STREQUALS2(str + 1, "0o", 2) || STREQUALS2(str + 1, "0O", 2));
        i = 3;
    } else {
        is_negative = false;
        assert(STREQUALS2(str, "0o", 2) || STREQUALS2(str, "0O", 2));
        i = 2;
    }

    for (i = 0; str[i]; i++) {
        if ('0' > str[i] || str[i] > '7') {
            print_bad_line_snippet(str, i, 1);
            print_parse_message(status, "error: '%c' is not an octal digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value *= 010;
        value += str[i] - '0';
    }

    if (is_negative) {
        value = -value;
    }

    status->success = true;
    return value;
}
/*
 * Full regex:
 * [+-]?[0-9]+
 */
int32_t parse_decimal_number(const char *str, struct parse_status *status)
{
    assert(!STREMPTY(str));

    long value = 0;
    size_t i;
    bool is_negative;

    if (str[0] == '-') {
        is_negative = true;
        i = 1;
    } else if (str[0] == '+') {
        is_negative = false;
        i = 1;
    } else {
        is_negative = false;
        i = 0;
    }

    for (; str[i]; i++) {
        if (!isdigit(str[i])) {
            print_parse_message(status, "error: '%c' is not a digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value *= 10;
        value += str[i] - '0';
    }

    if (is_negative) {
        value = -value;
    }

    if (MIN_SIGNED_INT32_VALUE > value || value > MAX_SIGNED_INT32_VALUE) {
        print_parse_message(status, "error: Decimal number is too wide to fit into a 32-bit word: %s\n",
                str);
        status->success = false;
        return 0;
    }

    status->success = true;
    return value;
}

/*
 * Full regex:
 * '[^\\\n]|\\[\\'"nrtbfv0]|\\x[A-Fa-f0-9]{2}|\\[0-7]{3}'
 */
char parse_character_literal(const char *str, struct parse_status *status)
{
    assert(str[0] == '\'');

    size_t len = strlen(str);
    if (len > 6) {
        print_bad_line_snippet(str, 1, len);
        print_parse_message(status, "error: Invalid character: %s\n", str);
        status->success = false;
        return 0;
    } else if (str[len - 1] != '\'') {
        print_bad_line_snippet(str, len - 1, 1);
        print_parse_message(status, "error: Expected single quote before end of character literal: %s\n",
                str);
        status->success = false;
        return 0;
    }

    size_t consumed_bytes;
    char result = parse_character(str + 1, &consumed_bytes, status);
    if (!status->success) {
        return result;
    }

    if (consumed_bytes < len - 2) {
        print_bad_line_snippet(str, 1, consumed_bytes);
        print_parse_message(status, "error: Character literal too long: %s\n", str);
        status->success = false;
        return 0;
    } else if (consumed_bytes > len - 2) {
        print_bad_line_snippet(str, 1, consumed_bytes);
        print_parse_message(status, "error: Character literal too short: %s\n", str);
        status->success = false;
        return 0;
    }

    return result;
}

static char parse_character(const char *str, size_t *length, struct parse_status *status)
{
    if (str[0] == '\\') {
        if (STREMPTY(str + 1)) {
            print_bad_line_snippet(str, 1, 1);
            print_parse_message(status, "error: Expected escape after '\\'.\n");
            status->success = false;
            return 0;
        }

        switch (str[1]) {
            case '\0':
                print_bad_line_snippet(str, 1, 1);
                print_parse_message(status, "error: Unexpected end of character literal: %s.\n",
                        str);
                status->success = false;
                return 0;
            case '\'':
            case '\"':
            case '\\':
                *length = 2;
                return str[1];
            case 'n':
                *length = 2;
                return '\n';
            case 'r':
                *length = 2;
                return '\r';
            case 't':
                *length = 2;
                return '\t';
            case 'b':
                *length = 2;
                return '\b';
            case 'f':
                *length = 2;
                return '\f';
            case 'v':
                *length = 2;
                return '\v';
            case '0':
                if (str[2] == '\0') {
                    print_bad_line_snippet(str, 2, 1);
                    print_parse_message(status, "error: Unexpected end of character literal: %s.\n",
                            str);
                    status->success = false;
                    return 0;
                } else if (!isdigit(str[2])) {
                    status->success = true;
                    *length = 2;
                    return '\0';
                }

                /* FALLTHROUGH */
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                /* Octal char */
                *length = 4;
                return parse_octal(str + 1, 3, status);
            case 'x':
            case 'X':
                /* Hex char */
                *length = 4;
                return parse_hex(str + 2, 2, status);
            default:
                print_bad_line_snippet(str, 1, 3);
                print_parse_message(status, "error: Bad escape: %s.\n", str);
                status->success = false;
                return 0;
        }
    } else {
        status->success = true;
        *length = 1;
        return str[0];
    }
}

static char parse_octal(const char *str, size_t length, struct parse_status *status)
{
    char value = 0;
    size_t i;

    for (i = 0; str[i]; i++) {
        if (i >= length) {
            status->success = true;
            return value;
        } else if ('0' > str[i] || str[i] > '7') {
            print_bad_line_snippet(str, i, 1);
            print_parse_message(status, "error: '%c' is not an octal digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value *= 010;
        value += str[i] - '0';
    }

    status->success = true;
    return value;
}

static char parse_hex(const char *str, size_t length, struct parse_status *status)
{
    char value = 0;
    size_t i;

    for (i = 0; str[i]; i++) {
        if (i >= length) {
            status->success = true;
            return value;
        } else if (!isxdigit(str[i])) {
            print_bad_line_snippet(str, i, 1);
            print_parse_message(status, "error: '%c' is not an hex digit.\n", str[i]);
            status->success = false;
            return 0;
        }

        value *= 0x10;
        value += (isdigit(str[i])) ? (str[i] - '0') : (tolower(str[i]) - 'a' + 10);
    }

    status->success = true;
    return value;
}

/*
 * Full regex:
 * "([^\\\n]|\\[\\'"nrtbfv0]|\\x[A-Fa-f0-9]{2}|\\[0-7]{3})*"
 */
char *parse_string_literal(const char *str, size_t *length, struct parse_status *status)
{
    assert(str[0] == '\"');
    char *buf = malloc(DEFAULT_STRING_CAPACITY * sizeof(char));
    size_t capacity = DEFAULT_STRING_CAPACITY;
    *length = 0;

    if (buf == NULL) {
        status->success = false;
        return NULL;
    }

    size_t index = 1;
    size_t consumed;
    char result;

    while (str[index] != '\"') {
        result = parse_character(str + index, &consumed, status);

        if (!status->success) {
            free(buf);
            return NULL;
        }

        index += consumed;
        buf[(*length)++] = result;

        if (str[index] == '\0') {
            print_bad_line_snippet(str, index, 1);
            print_parse_message(status, "error: Unexpected end of line while parsing string: %s.\n",
                    str);
            free(buf);
            status->success = false;
            return NULL;
        }

        /* Increase size of char buffer */
        if (*length >= capacity) {
            capacity *= 2;
            char *new_buf = malloc(capacity * sizeof(char));

            if (new_buf == NULL) {
                free(buf);
                status->success = false;
                return NULL;
            }

            strcpy(new_buf, buf);
            free(buf);
            buf = new_buf;
        }
    }

    buf[*length] = '\0';

    status->success = true;
    return buf;
}

int print_parse_message(const struct parse_status *status, const char *format, ...)
{
    int result;
    va_list args;
    va_start(args, format);
    result = fprintf(stderr, "%s:%zd: ", status->filename, status->lineno);

    if (result < 0) {
        return result;
    }

    result += vfprintf(stderr, format, args);
    va_end(args);
    return result;
}

bool string_equals_ignore_case(const char *a, const char *b)
{
    /* Assumes b is all lower-case */

    size_t i;
    for (i = 0; a[i] && b[i]; i++) {
        if ((a[i] != b[i]) && (a[i] == '\0' || b[i] == '\0')) {
            /* Strings are different lengths */
            return false;
        } else if (tolower(a[i]) != b[i]) {
            /* Characters not equal */
            return false;
        }
    }

    return true;
}

void print_bad_line_snippet(const char *line, size_t start, size_t length)
{
    puts(line);

    if (length == 0) {
        return;
    }

    size_t i;
    for (i = 0; i < start; i++) {
        putchar(' ');
    }

    putchar('^');

    for (i = 0; i < length - 1; i++) {
        putchar('~');
    }
}

