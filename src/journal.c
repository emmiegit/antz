/*
 * journal.c
 *
 * antz - My fictional assembly language and emulator.
 * Copyright (c) 2016 Ammon Smith
 *
 * antz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * antz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with antz.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "main.h"
#include "journal.h"

/* Static variables */
static FILE *journal_fh = NULL;

/* Function implementations */
void open_journal(const char *filename)
{
    journal_fh = fopen(filename, "w");

    if (journal_fh == NULL) {
        fprintf(stderr, "Unable to open journal file \"%s\": %s.\n",
                filename, strerror(errno));
        cleanup(1);
    }
}

void close_journal()
{
    if (journal_fh) {
        int ret = fclose(journal_fh);

        if (ret) {
            fprintf(stderr, "Unable to close journal file handle: %s.\n",
                    strerror(errno));
        }
    }
}

int journal(const char *format, ...)
{
    if (journal_fh == NULL) {
        return 0;
    }

    int result;
    va_list args;
    va_start(args, format);
    result = vfprintf(journal_fh, format, args);
    va_end(args);
    return result;
}

