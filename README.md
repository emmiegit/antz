## antz
An assembler and emulator for a fictional 32-bit architecture called "antz" (pronounced ant-zee).
This document gives the specifications for both the emulator and the architecture.

## Compilation
You can build the emulator by invoking `make`. The resultant binary will be `build/antz`.

## Usage


## Syntax
Comments begin with `;`.
The following literal values are accepted:
* Decimal numbers
* Binary numbers (in the form `0b0000`...)
* Octal numbers (in the form `0o0000`...)
* Hexadecimal numbers (in the form `0x0000`...)
* Characters (in the form `'a'`, includes escapes)
* Strings (in the form `"abc"`, includes escapes)

Each line is in the form "label: opcode operand1, operand2, operand3...". <br>
The label is optional, and it is also possible to have a label with no
instruction. Each operand must end with a comma if there is another operand
after. The language is case-insensitive with the exception  of labels.

## Memory
The system uses 32-bit word-addressable memory, and therefore has access to 2<sup>32</sup> words
(2 <sup>32</sup> * 4 bytes) of memory. However, not all of these addresses are available for general usage.

Memory Map:
(todo: add mode table)
```
Start address | End address   | Size      | Description              |
------------- | ------------- | --------- | ------------------------ |
0x0000 0000   | 0x0000 000f   | 60 bytes  | Peripherals table        |
0x0000 0010   | 0x0000 0fff   | 4 KB      | Trap vector table        |
0x0000 1000   | 0x0000 1fff   | 260 KB    | Executable page table    |
0x0000 2001   | 0xffff fdff   | 17.2 GB   | Available to the user    |
0xffff fe00   | 0xffff ffef   | 2 KB      | System service routines  |
0xffff fff0   | 0xffff ffff   | 60 bytes  | Device register mappings |
```
(Note that the starting address of the program is written to `0x2000` during assembly).

### Paging
The virtual memory subsystem divides it into pages of 1K each. Nonexistant pages are assumed
to be full of zeroes, and will only be created if written to with a nonzero value.

### Mode
Each page is either marked as executable or not. Executable pages are stored in the executable page table.
If a page is not found in the table when trying to execute it, `rer` is set and the emulator halts. Page 0
cannot be set to be exectuable.

To ignore modes, use the emulator's `-f` option.

### Peripherals table
Each memory location contains the address of the memory-mapped register for the peripheral,
or `0x0000` if the device is disconnected. See "devices".

### Trap vector table
(todo)

### Interrupt vector table
(todo)

### Interrupt stack
(todo)

## Devices
The emulator provides a few different peripherals. Here are the list
of auto-plugged devices and their correpsonding address in the peripheral
table:

* Keyboard (`0x0000`)
* Monitor (`0x0001`)
* Hard drive seek (`0x0002`)
* Hard drive data (`0x0003`)

At each location in the peripheral table is written the address of its memory-mapped registers. These
locations in memory behave like special registers rather than real memory. They should **not** be used
like normal general-purpose memory.

### Keyboard
The standard input stream is represented by the emulator as a keyboard. Reading from the register will
either give you the next buffered character or `'\0'` if there is no character available. Writing a
value to the register will signal to the keyboard that the system is ready to recieve another character.
Note that the value written to the register is lost.

### Monitor
The standard output stream is represented by the emulator as a monitor. Reading from the register will
tell you the state of the monitor. If it's a 1, then the monitor is busy, and if it's a 0, the monitor is
free. If the monitor is free, you may write a character to print to the register. Writing to the register
when it reports itself as busy will produce undefined behavior.

### Hard drive
(todo)

## Registers
All registers are listed in order, starting with id 0.

 General-purpose registers: <br>
`r0`, `r1`, `r2`, `r3`, `r4`, `r5`, <br>
`r6`, `r7`, `r8`, `r9`, `r10`, `r11`

Special registers:
* `rio` - I/O register.
* `rrt` - Return register, contains address to return to.
* `rer` - Error numebr register.
* `rpc` - The program counter. (read-only)

### rio
This register is used in various I/O system calls.

### rrt
This register is used to backup the `rpc` when making calls.

### rer
This register is set when errors occur. Here are the possible values:
* 0 - ok
* 1 - illegal operation
* 2 - arithmetic exception (divide by zero, etc.)
* 3 - i/o error
* 4 - tty error
* 5 - permission denied
* 6 - cannot execute (page is not marked executable)
* 7 - out of memory
* 999 - internal emulator error (should never happen)

### rpc
This register is the program counter, and stores the current location of
the program in memory. Attempts to directly set this register will result
in `rer` being set to 1.

## Opcodes
At a glance, here are all the opcodes and their ids in octal:
* `add` - 00
* `and` - 01
* `call` - 76
* `cmp` - 05
* `div` - 06
* `fadd` - 60
* `fcmp` - 64
* `fdiv` - 61
* `fmul` - 62
* `fneg` - 63
* `jez` - 51
* `jge` - 55
* `jgz` - 53
* `jle` - 56
* `jlz` - 52
* `jmp` - 50
* `jnz` - 54
* `jro` - 57
* `ld`  - 30
* `ldb` - 31
* `ldi` - 32
* `ldr` - 33
* `lea` - 34
* `lshf` - 13
* `mod` - 07
* `mov` - (alias)
* `movb` - 21
* `mul` - 10
* `nop` - (alias)
* `not` - 02
* `or`  - 03
* `ret` - (alias)
* `rshf` - 12
* `set` - 11
* `st`  - 40
* `stb` - 41
* `sti` - 42
* `str` - 43
* `sub` - 04
* `trap` - 77
* `xor` - 20

### add
`add [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]` <br>
Puts the sum of `[REG A]` and `[REG B]` in register `[DEST]`.

`add [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]` <br>
Puts the sum of `[REG A]` and the value `[VALUE]` in register `[DEST]`.

### and
`and [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]` <br>
Puts the logical and of `[REG A]` and `[REG B]` in register `[DEST]`.

`add [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]` <br>
Puts the logical and of `[REG A]` and the value `[VALUE]` in register `[DEST]`.

### call
`call [LABEL]` <br>
`[opcode 6] 1 [offset 25]` <br>
Sets `rrt` to `rpc` and jumps to the offset.

`call [REG]` <br>
`[opcode 6] 0 [reg 4] [unused 21]` <br>
Sets `rrt` to `rpc` and jumps to the location specifed by `[REG]`.

### cmp
`cmp [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Compares the signed values in `[REG A]` and `[REG B]`, and puts the
result in `[DEST]`.

-1 if `[REG A] < [REG B]`
 0 if `[REG A] = [REG B]`
 1 if `[REG A] > [REG B]`

### div
`div [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Calculates `[REG A] / [REG B]` and stores the result in `[DEST]`. Sets
`rer` if an arithmetic exception occurs.

### fadd
`fadd [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Uses floating-point arithmetic to compute the sum of `[REG A]` and `[REG B]`
and put it in register `[DEST]`.

### fcmp
`fcmp [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Compares the floating-point values in `[REG A]` and `[REG B]`, and puts the
result in `[DEST]`.

-1 if `[REG A] < [REG B]`
 0 if `[REG A] = [REG B]`
 1 if `[REG A] > [REG B]`

### fdiv
`fdiv [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Uses floating-point arithmetic to calculate `[REG A] / [REG B]` and stores the result in `[DEST]`. Sets `rer` if an arithmetic exception
occurs.

### fmul
`fmul [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Uses floating-point arithmetic to calculate `[REG A] * [REG B]` and stores the result in `[DEST]`.

### fneg
`fmul [DEST], [REG]` <br>
`[opcode 6] [dest 4] [reg 4] [unused 20]` <br>
Uses floating-point arithmetic to calculate `-[REG]` and stores the result in `[DEST]`.

### jez
`jez [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] == 0`, then jump to the signed offset. `rrt` is not set.

### jge
`jge [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] >= 0`, then jump to the signed offset. `rrt` is not set.

### jgz
`jgz [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] > 0`, then jump to the signed offset. `rrt` is not set.

### jle
`jle [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] <= 0`, then jump to the signed offset. `rrt` is not set.

### jlz
`jlz [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] < 0`, then jump to the signed offset. `rrt` is not set.

### jmp
`jmp [LABEL]` <br>
`[opcode 6] 1 [offset 25]` <br>
Unconditionally jumps to the offset. `rrt` is not set.

`jmp [REG]` <br>
`[opcode 6] 0 [reg 4] [unused 21]` <br>
Unconditionally jumps to the location specified by `[REG]`. `rrt` is not set.

### jnz
`jnz [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]` <br>
If `[REG] != 0`, then jump to the signed offset. `rrt` is not set.

### jro
`jro [REG]` <br>
`[opcode 6] [reg 4] [ignored 22]` <br>
Unconditionally jumps to the offset specified by `[REG]`. `rrt` is not set.
This instruction differs from the second flavor of `jmp` because it is _relative_
offset is used rather than an absolute address.

### ld
`ld [DEST], [LABEL]` <br>
`[opcode 6] [dest 4] [offset 22]` <br>
Loads the value stored at `[LABEL]` into `[DEST]`.
In register transfer notation: `[DEST] <- mem(LABEL)`.

### ldb
`ldb [DEST], [BYTE], [LABEL]` <br>
`[opcode 6] [dest 4] [value 2] [offset 20]` <br>
Loads the given byte at `[LABEL]` into `[DEST]`.
Say the value at `[LABEL]` is `0xfe00abcd`:
If `[BYTE]` is 0, then `[DEST]` is set to `0xcd`,
but if `[BYTE]` is 3, then `[DEST]` is set to `0xfe`.

### ldi
`ldi [DEST], [LABEL]` <br>
`[opcode 6] [dest 4] [offset 22]` <br>
Loads the value referred to at `[LABEL]` into `[DEST]`.
In register transfer notation: `[DEST] <- mem(mem(LABEL))`

### ldr
`ldr [DEST], [REG], [VALUE]` <br>
`[opcode 6] [dest 4] [reg 4] [offset 18]` <br>
Loads the value at the location in `[REG]`, incremented by `[VALUE]`.
In register transfer notation: `[DEST] <- mem(LABEL + VALUE)`.

### lea
`lea [DEST], [VALUE]` <br>
`[opcode 6] [offset 26]` <br>
Loads the address of `[LABEL]` into `[DEST]`.
In register transfer notation: `[DEST] <- [LABEL]`.

### lshf
`lshf [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]` <br>
Leftshifts `[REG A]` `[REG B]` times and puts the result in register `[DEST]`.

`lshf [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]` <br>
Leftshifts `[REG A]` `[VALUE]` times and puts the result in register `[DEST]`.

### mod
`mod [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]` <br>
Calculates `[REG A] % [REG B]` and stores the result in `[DEST]`. Sets
`rer` if an arithmetic exception occurs.

### mov
`mov [DEST], [REG]` <br>
Copies the value of `[REG]` to `[DEST]`.
This instruction is really an alias for `add [DEST] [REG] 0`.

### movb
`movb [DEST], [REG], [DEST BYTE], [SRC BYTE]`<br>
`[opcode 6] [dest 4] [reg 4] [dest byte 2] [src byte 2] [unused 14]
Copies the byte at `[REG]` to the byte at `[DEST]`.
Say `[DEST]` is `0xfe0000ab` and `[REG]` is `0x1100ffff`.
If `[DEST BYTE]` is 1 and `[SRC BYTE]` is 3, then `[DEST]` would become
`0xfe0011ab`.

### mul
`mul [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] [reg b 4] [unused 16]`
Calculates `[REG A] * [REG B]` and stores the result in `[DEST]`.

### nop
`nop` <br>
Performs no operation.
This instruction is really an alias for `add r0, r0, 0` or `.fill 0`.

### not
`not [DEST], [REG]` <br>
`[opcode 6] [dest 4] [reg 4] [unused 18]`
Negates the value in `[REG]` and stores it in `[DEST]`.

### or
`or [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]`
Puts the logical or of `[REG A]` and `[REG B]` in register `[DEST]`.

`or [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]`
Puts the logical or of `[REG A]` and the value `[VALUE]` in register `[DEST]`.

### ret
`ret` <br>
Returns to the calling address and resumes execution there.
Alias for `jmp rrt`.

### rshf
`rshf [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]`
Rightshifts `[REG A]` `[REG B]` times and puts the result in register `[DEST]`.

`rshf [DEST], [REG A] [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]`
Rightshifts `[REG A]` `[VALUE]` times and puts the result in register `[DEST]`.

### set
`set [DEST], [VALUE]` <br>
`[opcode 6] [dest 4] [value 22]`

Sets the `[DEST]` to `[VALUE]`.

### st
`st [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]`
Stores the value in `[REG]` at `[LABEL]`.
In register transfer notation: `mem(LABEL) <- [REG]`.

### stb
`stb [REG], [BYTE], [LABEL]` <br>
`[opcode 6] [reg 4] [value 2] [offset 20]`
Treats `[REG]` as one byte (i.e. masking it with `0xff`) and
setting the given byte to that value.
Say `[REG]` is `0xabcde00fe`. If `[BYTE]` is 0, then `[LABEL]`
is set to `0x------fe`, where dashes indicate unchanged nibbles.
Likewise, if `[BYTE]` is 3, then `[LABEL]` is set to
`0xfe------`.

### sti
`sti [REG], [LABEL]` <br>
`[opcode 6] [reg 4] [offset 22]`
Stores `[REG]` into the location referred to at `[LABEL]`.
In register transfer notation: `mem(mem(LABEL)) <- [REG]`.

### str
`str [REG A], [REG B], [VALUE]` <br>
`[opcode 6] [reg a 4] [reg b 4] [offset 18]`
Stores `[REG A]` into the location referred to in `[REG B]`, incremented
by `[VALUE]`.
In register transfer notation: `mem(REG B + VALUE) <- [REG A]`.

### sub
`sub [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]` <br>
Puts the difference of `[REG A]` and `[REG B]` in register `[DEST]`.

`sub [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]` <br>
Puts the difference of `[REG A]` and the value `[VALUE]` in register `[DEST]`.

### trap
`trap [CALL NUMBER]` <br>
`[opcode 6] [call number 12] [unused 14]`
Performs a system call with the given instruction id. See the section on system calls
for more information.

### xor
`xor [DEST], [REG A], [REG B]` <br>
`[opcode 6] [dest 4] [reg a 4] 1 [reg b 4] [unused 13]`
Puts the logical exclusive or of `[REG A]` and `[REG B]` in register `[DEST]`.

`xor [DEST], [REG A], [VALUE]` <br>
`[opcode 6] [dest 4] [reg a 4] 0 [value 17]`
Puts the logical exclusive or of `[REG A]` and the value `[VALUE]` in register `[DEST]`.

## System Calls
12 bits are allocated for the system call number. The range of valid call numbers goes from
`0x010` to `0xfff`.

Here is a summary of all the system calls available:
* `0x010`: GETC
* `0x011`: PUTC
* `0x012`: MVCUR
* `0x013`: CLSCR
* `0x021`: PUTS
* `0xfff`: HALT

### GETC
Reads one character from the user and places it in `rio`. If the end of the stream (i.e. EOF)
has been reached, then `rio` is set to -1. If an error occurs, `rer` is set to 5.

### PUTC
Prints the character in `rio`. Values above the value of a byte are truncated. If an error occurs,
`rer` is set to 5.

### MVCUR
The `rio` register is treated as two two-byte integers combined, the first referring to the
x-position and the second to the y-position. The cursor is then set to that location.
So if `rio` is `0x00030008`, then the cursor is set to (3, 8). If an error occurs, `rer` will
be set appropriately.

### CLSCR
Clears the screen.

### PUTS
Prints characters starting at the address in `rio` until a null byte (`\0`) is found.

### HALT
Ends program execution.

## Assembler instructions
(aka pseudo-ops)

### .addrof \[label\]
Fills the current word with the full address of the specified label.

### .asciiz \[string\]
Fills a null-terminated ASCII string into memory.

### .block \[size\]
Fill the next \[size\] words with zeroes.

### .data
Specifies the beginning of the data section.

### .define \[name\] \[value\]
Before assembling, replace all instances of \[name\] with \[value\].
(To be implemented)

### .extern \[name\]
Sets a label at this location and adds it to the symbol table.

### .end
Instructs the assembler to end creation of machine code.

### .fill \[value\]
Fill this word with the literal value specified.

### .text \(main\)
Specifies the beginning of executable instructions.
If "main" is specified then it also declares `.extern main` and writes
the address of this instruction to `0x2000`. Only one "main" can be specified.

### .orig \[location\]
Jump to this location to continue writing words.
(Note: the starting address for writing is `0x20001`)

### .zfill
Equivalent to `.fill 0`.

