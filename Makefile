# Makefile
#
# antz - My fictional assembly language and emulator.
# Copyright (c) 2016 Ammon Smith
#
# antz is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# antz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with antz.  If not, see <http://www.gnu.org/licenses/>.
#

.PHONY: all release install force debug forcedebug test man clean

CC = gcc
LEX = flex
YACC = bison
SRC_DIR = src
BUILD_DIR = build
DEST_DIR =
EXE = build/antz

SRC_EXT = c
OBJ_EXT = o
HDR_EXT = h
DEP_EXT = d
ASM_EXT = s
SOURCES = $(wildcard $(SRC_DIR)/*.$(SRC_EXT))
OBJECTS = $(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(SOURCES:.$(SRC_EXT)=.$(OBJ_EXT)))
DEPS = $(OBJECTS:.$(OBJ_EXT)=.$(DEP_EXT))
C_FLAGS = -std=c99 -pipe -O3
WARN_FLAGS = -pedantic -Wall -Wextra
INC = -I $(SRC_DIR)
LIB = -l readline

LEX_EXT = l
YY_EXT = yy.$(SRC_EXT)
LEX_SOURCES = $(wildcard $(SRC_DIR)/*.$(LEX_EXT))
LEX_OBJECTS = $(patsubst $(SRC_DIR)/%,$(SRC_DIR)/%,$(LEX_SOURCES:.$(LEX_EXT)=.$(YY_EXT)))
SOURCES += $(LEX_OBJECTS)
LEX_FLAGS =

YACC_EXT = y
YACC_SOURCES = $(SRC_DIR)/parser.$(YACC_EXT)
YACC_OBJECTS = $(SRC_DIR)/y.tab.$(HDR_EXT)
YACC_FLAGS =

all: $(EXE)

release:
	@echo '[RELEASE] $(notdir $(EXE))'
	@make clean $(EXE) EXTRA_FLAGS='-DNDEBUG -fstack-protector-strong'

$(EXE): $(YACC_OBJECTS) $(OBJECTS)
	@mkdir -p '$(BUILD_DIR)'
	@echo '[LN] $(notdir $(EXE))'

	@$(CC) $(C_FLAGS) $(WARN_FLAGS) $(EXTRA_FLAGS) $(LIB) $^ -o $(EXE)

$(BUILD_DIR)/%.$(OBJ_EXT): $(SRC_DIR)/%.$(SRC_EXT)
	@mkdir -p '$(BUILD_DIR)'
	@echo '[CC] $(@F)'
	@if [[ '$<' == *.$(YY_EXT) ]]; then \
		$(CC) $(C_NOWARN_FLAGS) $(EXTRA_FLAGS) $(LIB) $(INC) -c -o $@ $<; \
	else \
		$(CC) $(C_FLAGS) $(WARN_FLAGS) $(EXTRA_FLAGS) $(LIB) $(INC) -c -o $@ $<; \
	fi

$(YACC_OBJECTS): $(YACC_SOURCES)
	@mkdir -p '$(BUILD_DIR)'
	@echo '[YACC] $(@F)'
	@$(YACC) $(YACC_FLAGS) -o $@ $<

$(LEX_OBJECTS): $(LEX_SOURCES)
	@mkdir -p '$(BUILD_DIR)'
	@echo '[LEX] $(@F)'
	@$(LEX) $(LEX_FLAGS) -o $@ $<

man:
	@make -C man

clean:
	@make -C man clean
	@echo '[RMDIR] $(BUILD_DIR)'
	@rm -rf '$(BUILD_DIR)'
	@echo '[RM] $(LEX_OBJECTS)'
	@rm -f '$(LEX_OBJECTS)'
	@echo '[RM] $(YACC_OBJECTS)'
	@rm -f '$(YACC_OBJECTS)'

force:
	@make -C man force
	@echo '[RMDIR] $(BUILD_DIR)'
	@rm -rf '$(BUILD_DIR)'
	@make $(EXE)

debug:
	@make $(EXE) EXTRA_FLAGS='-Og -g -UNDEBUG'

forcedebug: clean debug

-include: $(DEPS)
